package by.nesterenya.freemodeling;

import java.awt.AlphaComposite;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.SplashScreen;
import java.awt.SystemColor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.plaf.DimensionUIResource;
import javax.swing.table.DefaultTableModel;

import by.nesterenya.analysis.*;
import by.nesterenya.boundary.Boundary;
import by.nesterenya.boundary.EvenlyDistributedLoad;
import by.nesterenya.boundary.ILoad;
import by.nesterenya.boundary.StaticTemperature;
import by.nesterenya.boundary.Support;
import by.nesterenya.controls.NiButton;
import by.nesterenya.elements.material.Material;
import by.nesterenya.geometry.Box;
import by.nesterenya.glviewers.GlViewer;
import by.nesterenya.glviewers.GlViewer.DisplayType;
import by.nesterenya.mesh.Mesh;
import by.nesterenya.solvers.StaticStructalSolver;
import by.nesterenya.solvers.ThermalDynamicSolver;

import com.jogamp.opengl.util.FPSAnimator;

import javax.swing.JTable;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.border.CompoundBorder;
import javax.swing.UIManager;

// -splash:src/images/splash.gif


public class GeneralWindow implements ActionListener {
  boolean isRunAnimate = false;
  // Окно визуализатора
  final GlViewer displayViewer = new GlViewer();
  DefaultTableModel boundariesModel = new DefaultTableModel();
  DefaultTableModel loadsModel = new DefaultTableModel();
  Timer timer = null;
  final FPSAnimator animator = new FPSAnimator(displayViewer, 20, true);

  ThermalDynamicAnalisis thermalDynamic = null;
  StaticStructalAnalisis staticStranalysis;

  // TODO Удалить это
  public static Color buttonBg = new Color(205, 205, 225);

  /*
   * Методы для упрощения стилизации кнопок
   */

  public static NiButton setupButtonUI(AbstractButton button) {
    return setupButtonUI(button, -1);
  }

  public static NiButton setupButtonUI(AbstractButton button, Insets insets) {
    return setupButtonUI(button, -1, insets);
  }

  public static NiButton setupButtonUI(AbstractButton button, int rounded) {
    return setupButtonUI(button, Arrays.asList(rounded));
  }
//TODO добавить стрелки на координатные оси
  public static NiButton setupButtonUI(AbstractButton button, int rounded, Insets insets) {
    return setupButtonUI(button, Arrays.asList(rounded), insets);
  }

  public static NiButton setupButtonUI(AbstractButton button, java.util.List<Integer> rounded) {
    return setupButtonUI(button, rounded, new Insets(4, 4, 4, 4));
  }

  public static NiButton setupButtonUI(AbstractButton button, java.util.List<Integer> rounded,
      Insets insets) {
    NiButton stbui = new NiButton(button, false);
    stbui.setColor(buttonBg, 128);
    stbui.setRoundedSides(rounded);
    stbui.setAlwaysDrawBackground(true);
    button.setUI(stbui);
    button.setMargin(new Insets(1, 1, 1, 1));
    button.setBorder(BorderFactory.createEmptyBorder(insets.top, insets.left, insets.bottom,
        insets.right));
    return stbui;
  }

  public static NiButton setupDialogButtonUI(AbstractButton button, int rounded) {
    return setupDialogButtonUI(button, Arrays.asList(rounded));
  }

  public static NiButton setupDialogButtonUI(AbstractButton button, java.util.List<Integer> rounded) {
    return setupDialogButtonUI(button, rounded, new Insets(4, 12, 4, 12));
  }

  public static NiButton setupDialogButtonUI(AbstractButton button, int rounded, Insets insets) {
    return setupDialogButtonUI(button, Arrays.asList(rounded), insets);
  }

  public static NiButton setupDialogButtonUI(AbstractButton button,
      java.util.List<Integer> rounded, Insets insets) {
    NiButton gbui = setupButtonUI(button, rounded, insets);
    gbui.setStaticBorderColor(new Color(155, 155, 155), new Color(175, 175, 175));
    gbui.setStaticColor(new Color(210, 210, 210), new Color(175, 175, 175));
    // button.setMargin ( new Insets ( 4, 12, 4, 12 ) );
    return gbui;
  }

  JTabbedPane tabbedPane;
  private JFrame frame;
  private JTextField tb_lenght;
  JLabel lb_testY;

  double lastX = 0;
  double lastY = 0;

  private boolean Am_I_In_FullScreen = false;
  private int PrevX, PrevY, PrevWidth, PrevHeight;

  private class FullScreenEffect implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent arg0) {
       if (Am_I_In_FullScreen == false) {

        PrevX = frame.getX();
        PrevY = frame.getY();
        PrevWidth = frame.getWidth();
        PrevHeight = frame.getHeight();

        frame.dispose(); // Destroys the whole JFrame but keeps organized every Component
        // Needed if you want to use Undecorated JFrame
        // dispose() is the reason that this trick doesn't work with videos
        frame.setUndecorated(true);

        frame.setBounds(-10, -100, frame.getToolkit().getScreenSize().width + 30, frame
            .getToolkit().getScreenSize().height + 110);
        frame.setVisible(true);
        Am_I_In_FullScreen = true;
      } else {
        frame.setVisible(true);

        frame.setBounds(PrevX, PrevY, PrevWidth, PrevHeight);
        frame.dispose();
        frame.setUndecorated(false);
        frame.setVisible(true);
        Am_I_In_FullScreen = false;
      }
    }
  }

  /**
   * Launch the application.
   */
  public static void main(String[] args) {
    EventQueue.invokeLater(new Runnable() {
      public void run() {

        try {



          GeneralWindow window = null;// new GeneralWindow();

          // TODO обратотать на случай ошибки
          final SplashScreen splash = SplashScreen.getSplashScreen();
          if (splash == null) {
            System.out.println("SplashScreen.getSplashScreen() returned null");
            return;
          }
          Graphics2D g = splash.createGraphics();
          if (g == null) {
            System.out.println("g is null");
            return;
          }

          for (int i = 0; i < 50; i++) {
            if (i == 20) {
              window = new GeneralWindow();
            }
            renderSplashFrame(g, i);
            splash.update();
            try {
              Thread.sleep(20);
            } catch (InterruptedException e) {}
          }
          splash.close();



          if (window == null) {
            window = new GeneralWindow();
          }

          window.frame.setVisible(true);
          window.frame.setExtendedState(Frame.MAXIMIZED_BOTH);

        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    });
  }

  /**
   * Create the application.
   */
  public GeneralWindow() {
    initialize();
  }

  private void refreshLoadsTable() {



    loadsModel.setColumnCount(0);
    loadsModel.setRowCount(0);
    loadsModel.addColumn("Граничное условие");

   if(thermalDynamic!=null) {
    for (Object obj : thermalDynamic.getLoads()) {
      // Append a row
      loadsModel.addRow(new Object[] {obj});

      System.out.println(obj);
    }
   }
   
   //TODO удалить и объеденить с выше
   if(staticStranalysis!=null) {
     for (Object obj : staticStranalysis.getLoads()) {
       // Append a row
       loadsModel.addRow(new Object[] {obj});

       System.out.println(obj);
     }
    }

  }

  private JTextField tb_width;
  private JTextField tb_height;
  private JTextField tb_nodeOZ;
  private JTextField tb_nodeOX;
  private JTextField tb_nodeOY;
  private JTable tbl_boundaries;
  private JTable table_loads;
  private JTextField textField;
  private JTextField textField_1;
  private JTextField textField_2;
  private JTextField textField_3;
  private JTextField textField_4;
  private JTextField tb_intitialTemperature;
  private JTextField tb_timeStep;
  private JTextField tb_timeStudy;
  private JTextField tb_kaf;
  private JTextField tb_alp;

  /**
   * Initialize the contents of the frame.
   */
  private void initialize() {
    frame = new JFrame();
    frame
        .setTitle("\u041C\u043E\u0434\u0435\u043B\u0438\u0440\u043E\u0432\u0430\u043D\u0438\u0435");
    frame.setBounds(100, 100, 658, 450);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.getContentPane().setLayout(new GridLayout(0, 1, 0, 0));

    final JSplitPane splitPane = new JSplitPane();
    frame.getContentPane().add(splitPane);
    // panel_draw.setRealized(true);
    // panel_draw.setAutoSwapBufferMode(false);

    // panel_draw.setBackground(Color.red);// setLayout(new GridLayout(1, 0,
    // 0,
    // 0));

    tabbedPane = new JTabbedPane(JTabbedPane.TOP);



    tabbedPane.setForeground(SystemColor.inactiveCaptionText);
    tabbedPane.setBackground(SystemColor.inactiveCaptionBorder);
    tabbedPane.setAlignmentX(0.0f);
    tabbedPane.setAlignmentY(0.0f);
    tabbedPane.setPreferredSize(new Dimension(100, 100));
    // tabbedPane.setSelectedIndex(0);
    tabbedPane.setFont(new Font("Verdana", Font.PLAIN, 11));
    splitPane.setLeftComponent(tabbedPane);

    MouseWheelListener mouseWhellListener = new MouseWheelListener() {

      @Override
      public void mouseWheelMoved(MouseWheelEvent e) {
        // TODO перейменовать displayViewer
        displayViewer.addToZoom(-0.05 * e.getUnitsToScroll());
      }
    };
    
        JPanel panel_1 = new JPanel();
        panel_1.setForeground(UIManager.getColor("Button.foreground"));
        panel_1.setFont(new Font("Verdana", panel_1.getFont().getStyle(), panel_1.getFont().getSize()));
        panel_1.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        panel_1.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, new Color(51, 51, 51), new Color(
            204, 204, 51), new Color(102, 102, 102), new Color(102, 102, 102)));
        panel_1.setAlignmentY(1.0f);
        panel_1.setAlignmentX(1.0f);
        panel_1.setBackground(Color.WHITE);
        panel_1.setToolTipText("");
        tabbedPane.addTab("М", null, panel_1, "Модель");
        
        
            tb_lenght = new JTextField();
            tb_lenght.setColumns(10);
            lb_testY = new JLabel("Длина пластинки");
            
                JButton btn_createModel = new JButton("Создать модель");
                btn_createModel.addActionListener(new ActionListener() {
                  public void actionPerformed(ActionEvent e) {
                    final by.nesterenya.geometry.Box box =
                        new Box(Double.parseDouble(tb_lenght.getText()),
                            Double.parseDouble(tb_width.getText()), Double.parseDouble(tb_height.getText()));

                    thermalDynamic = new ThermalDynamicAnalisis();
                    thermalDynamic.setGeometry(box);

                    displayViewer.setThermalDynamic(thermalDynamic);


                    displayViewer.setTypeDisplay(DisplayType.MODEL);
                    displayViewer.display();
                  }
                });
                
                
                    JLabel label = new JLabel("Ширана пластинки");
                    
                        tb_width = new JTextField();
                        tb_width.setColumns(10);
                        
                            JLabel f = new JLabel("Толщина пластинки");
                            
                                tb_height = new JTextField();
                                tb_height.setColumns(10);
                                
                                //TODO удалить эту кнопку
                                JButton button_1 = new JButton("НДС Модель");
                                button_1.addActionListener(new ActionListener() {
                                  public void actionPerformed(ActionEvent arg0) {
                                    
                                    
                                    final by.nesterenya.geometry.Box box =
                                        new Box(Double.parseDouble(tb_lenght.getText()),
                                            Double.parseDouble(tb_width.getText()), Double.parseDouble(tb_height.getText()));

                                    staticStranalysis = new StaticStructalAnalisis();
                                    staticStranalysis.setGeometry(box);

                                    displayViewer.setStructalAnalysis(staticStranalysis);

                                    displayViewer.setTypeDisplay(DisplayType.MODEL);
                                    displayViewer.display();
                                  }
                                });
                                
                                tb_kaf = new JTextField();
                                tb_kaf.setColumns(10);
                                
                                JButton btnNewButton_4 = new JButton("изменить коф");
                                btnNewButton_4.addActionListener(new ActionListener() {
                                  public void actionPerformed(ActionEvent e) {
                                    displayViewer.setCorrectKaf(Double.parseDouble(tb_kaf.getText()));
                                  }
                                });
                                
                                    GroupLayout gl_panel_1 = new GroupLayout(panel_1);
                                    gl_panel_1.setHorizontalGroup(
                                      gl_panel_1.createParallelGroup(Alignment.TRAILING)
                                        .addGroup(gl_panel_1.createSequentialGroup()
                                          .addContainerGap()
                                          .addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
                                            .addComponent(btn_createModel, GroupLayout.DEFAULT_SIZE, 172, Short.MAX_VALUE)
                                            .addComponent(tb_height, GroupLayout.DEFAULT_SIZE, 172, Short.MAX_VALUE)
                                            .addComponent(tb_width, GroupLayout.DEFAULT_SIZE, 172, Short.MAX_VALUE)
                                            .addComponent(tb_lenght, GroupLayout.DEFAULT_SIZE, 172, Short.MAX_VALUE)
                                            .addComponent(lb_testY)
                                            .addComponent(label)
                                            .addComponent(f)
                                            .addComponent(button_1, GroupLayout.DEFAULT_SIZE, 172, Short.MAX_VALUE)
                                            .addGroup(gl_panel_1.createSequentialGroup()
                                              .addComponent(tb_kaf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                              .addPreferredGap(ComponentPlacement.RELATED)
                                              .addComponent(btnNewButton_4)))
                                          .addContainerGap())
                                    );
                                    gl_panel_1.setVerticalGroup(
                                      gl_panel_1.createParallelGroup(Alignment.LEADING)
                                        .addGroup(gl_panel_1.createSequentialGroup()
                                          .addContainerGap()
                                          .addComponent(lb_testY)
                                          .addPreferredGap(ComponentPlacement.RELATED)
                                          .addComponent(tb_lenght, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                          .addPreferredGap(ComponentPlacement.UNRELATED)
                                          .addComponent(label)
                                          .addPreferredGap(ComponentPlacement.RELATED)
                                          .addComponent(tb_width, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                          .addPreferredGap(ComponentPlacement.UNRELATED)
                                          .addComponent(f)
                                          .addPreferredGap(ComponentPlacement.RELATED)
                                          .addComponent(tb_height, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                          .addPreferredGap(ComponentPlacement.UNRELATED)
                                          .addComponent(btn_createModel)
                                          .addGap(36)
                                          .addComponent(button_1)
                                          .addPreferredGap(ComponentPlacement.UNRELATED)
                                          .addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
                                            .addComponent(tb_kaf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                            .addComponent(btnNewButton_4))
                                          .addContainerGap(72, Short.MAX_VALUE))
                                    );
                                    panel_1.setLayout(gl_panel_1);

    JPanel panel_2 = new JPanel();
    tabbedPane.addTab("4", null, panel_2, null);

    JLabel lblNewLabel = new JLabel("Узлов по оси ОХ");

    JLabel lblNewLabel_1 = new JLabel("Узлов по оси ОУ");

    JLabel lblNewLabel_2 = new JLabel("Узлов по оси ОZ");

    tb_nodeOZ = new JTextField();
    tb_nodeOZ.setColumns(10);

    tb_nodeOX = new JTextField();
    tb_nodeOX.setColumns(10);

    tb_nodeOY = new JTextField();
    tb_nodeOY.setColumns(10);

    JButton btn_showMesh = new JButton("Показать сетку");
    btn_showMesh.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
     // final by.nesterenya.geometry.Box box =
        // new Box(Double.parseDouble(tb_lenght.getText()),
        // Double.parseDouble(tb_width.getText()), Double.parseDouble(tb_height.getText()));


        // TODO проверка на нулл геометрии
        Mesh mesh =
            new Mesh(thermalDynamic.getGeometry(), Integer.parseInt(tb_nodeOX.getText()), Integer
                .parseInt(tb_nodeOY.getText()), Integer.parseInt(tb_nodeOZ.getText()));

        thermalDynamic.setMesh(mesh);

        displayViewer.setThermalDynamic(thermalDynamic);
        displayViewer.setTypeDisplay(DisplayType.MESH);
        displayViewer.display();
      }
    });
   
    
    JButton btnNewButton_3 = new JButton("НДС Сетка");
    btnNewButton_3.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        
     // final by.nesterenya.geometry.Box box =
        // new Box(Double.parseDouble(tb_lenght.getText()),
        // Double.parseDouble(tb_width.getText()), Double.parseDouble(tb_height.getText()));


        // TODO проверка на нулл геометрии
        Mesh mesh =
            new Mesh(staticStranalysis.getGeometry(), Integer.parseInt(tb_nodeOX.getText()), Integer
                .parseInt(tb_nodeOY.getText()), Integer.parseInt(tb_nodeOZ.getText()));

        staticStranalysis.setMesh(mesh);

        displayViewer.setStructalAnalysis(staticStranalysis);
        displayViewer.setTypeDisplay(DisplayType.MESH);
        displayViewer.display();
        
      }
    });
    GroupLayout gl_panel_2 = new GroupLayout(panel_2);
    gl_panel_2.setHorizontalGroup(
      gl_panel_2.createParallelGroup(Alignment.LEADING)
        .addGroup(gl_panel_2.createSequentialGroup()
          .addContainerGap()
          .addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
            .addComponent(tb_nodeOX, GroupLayout.DEFAULT_SIZE, 178, Short.MAX_VALUE)
            .addComponent(lblNewLabel)
            .addComponent(lblNewLabel_1)
            .addComponent(lblNewLabel_2)
            .addComponent(tb_nodeOY, GroupLayout.DEFAULT_SIZE, 178, Short.MAX_VALUE)
            .addComponent(tb_nodeOZ, GroupLayout.DEFAULT_SIZE, 178, Short.MAX_VALUE)
            .addComponent(btn_showMesh, GroupLayout.DEFAULT_SIZE, 178, Short.MAX_VALUE)
            .addComponent(btnNewButton_3, GroupLayout.DEFAULT_SIZE, 178, Short.MAX_VALUE))
          .addContainerGap())
    );
    gl_panel_2.setVerticalGroup(
      gl_panel_2.createParallelGroup(Alignment.LEADING)
        .addGroup(gl_panel_2.createSequentialGroup()
          .addContainerGap()
          .addComponent(lblNewLabel)
          .addPreferredGap(ComponentPlacement.RELATED)
          .addComponent(tb_nodeOX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          .addGap(7)
          .addComponent(lblNewLabel_1)
          .addPreferredGap(ComponentPlacement.RELATED)
          .addComponent(tb_nodeOY, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          .addPreferredGap(ComponentPlacement.UNRELATED)
          .addComponent(lblNewLabel_2)
          .addPreferredGap(ComponentPlacement.RELATED)
          .addComponent(tb_nodeOZ, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          .addPreferredGap(ComponentPlacement.UNRELATED)
          .addComponent(btn_showMesh)
          .addPreferredGap(ComponentPlacement.UNRELATED)
          .addComponent(btnNewButton_3)
          .addContainerGap(138, Short.MAX_VALUE))
    );
    panel_2.setLayout(gl_panel_2);

    JPanel panel_3 = new JPanel();
    tabbedPane.addTab("5", null, panel_3, null);

    JScrollPane scrollPane = new JScrollPane();

    JButton btnGetboundaries = new JButton("GetBoundaries");
    btnGetboundaries.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        if (thermalDynamic != null) {

          Object[] names = thermalDynamic.getMesh().getBoundaries().keySet().toArray();
          boundariesModel.setColumnCount(0);
          boundariesModel.setRowCount(0);
          boundariesModel.addColumn("Грань");


          for (Object obj : names) {
            // Append a row
            boundariesModel.addRow(new Object[] {(String) obj});

            System.out.println((String) obj);
          }


        }
      }
    });

    ;

    JButton button = new JButton("Добавить нагрузку");


    button.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent arg0) {

        JTextField force = new JTextField();

        final JComponent[] inputs = new JComponent[] {new JLabel("Распределенная нагрузка, Па"), force

        };

        // TODO сделать чтобы окошко всплывало слево
        int rezult =
            JOptionPane.showConfirmDialog(null, inputs, "Указание нагрузки",
                JOptionPane.OK_CANCEL_OPTION);

        if (rezult == JOptionPane.OK_OPTION) {
          if (force.getText().trim().length() == 0) {
            JOptionPane.showMessageDialog(null, "Поле не должно быть пустым", "Ошибка",
                JOptionPane.OK_OPTION);
          } else {
            // Код добавления температуры
            // TODO Переместить boundaries в figure

            Boundary boundary =
                staticStranalysis.getMesh().getBoundaries().get(staticStranalysis.getSelectedPlane());
           
            ILoad raspLoad =
                new EvenlyDistributedLoad(Double.parseDouble(force.getText().replace(',', '.')),
                    boundary);
            
            staticStranalysis.getLoads().add(raspLoad);
          }
        }

        refreshLoadsTable();

      }
    });
    // Сделать линии тонкими
    JButton btn_addTempereture = new JButton("Добавить темературу");
    btn_addTempereture.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent arg0) {


        JTextField tempereature = new JTextField();

        final JComponent[] inputs = new JComponent[] {new JLabel("Температура, К"), tempereature

        };

        // TODO сделать чтобы окошко всплывало слево
        int rezult =
            JOptionPane.showConfirmDialog(null, inputs, "Указание температуры",
                JOptionPane.OK_CANCEL_OPTION);

        if (rezult == JOptionPane.OK_OPTION) {
          if (tempereature.getText().trim().length() == 0) {
            JOptionPane.showMessageDialog(null, "Поле не должно быть пустым", "Ошибка",
                JOptionPane.OK_OPTION);
          } else {
            // Код добавления температуры
            // TODO Переместить boundaries в figure

            Boundary boundary =
                thermalDynamic.getMesh().getBoundaries().get(thermalDynamic.getSelectedPlane());
            ILoad thermalLoad =
                new StaticTemperature(Double.parseDouble(tempereature.getText().replace(',', '.')),
                    boundary);
            thermalDynamic.getLoads().add(thermalLoad);
          }
        }

        refreshLoadsTable();
      }
    });

    JButton button_2 = new JButton("Добавить закрепление");
    button_2.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        //JTextField force = new JTextField();

        final JComponent[] inputs = new JComponent[] {new JLabel("Закрепить?")
        };

        // TODO сделать чтобы окошко всплывало слево
        int rezult =
            JOptionPane.showConfirmDialog(null, inputs, "Указание закрепления",
                JOptionPane.OK_CANCEL_OPTION);

        if (rezult == JOptionPane.OK_OPTION) {
         
            // Код добавления температуры
            // TODO Переместить boundaries в figure

            Boundary boundary =
                staticStranalysis.getMesh().getBoundaries().get(staticStranalysis.getSelectedPlane());
           
            ILoad fixed =
                new Support(boundary);
            
            staticStranalysis.getLoads().add(fixed);
          
        }

        refreshLoadsTable();
      }
    });

    JScrollPane scrollPane_1 = new JScrollPane();

    JLabel lblNewLabel_3 = new JLabel("Граничные условия");

    JButton btnDeleteLoad = new JButton("X");
    btnDeleteLoad.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent arg0) {

        // TODO Проверку или выбрана строка
        ILoad load =
            (ILoad) loadsModel.getValueAt(table_loads.getSelectedRow(),
                table_loads.getSelectedColumn());
        thermalDynamic.getLoads().remove(load);
        refreshLoadsTable();

        // TODO Добавить подсветку при выбоере нагрузки
      }
    });

    btnDeleteLoad.setForeground(Color.RED);
    
    JButton btnGb = new JButton("НДК gb");
    btnGb.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        if (staticStranalysis != null) {

          Object[] names = staticStranalysis.getMesh().getBoundaries().keySet().toArray();
          boundariesModel.setColumnCount(0);
          boundariesModel.setRowCount(0);
          boundariesModel.addColumn("Грань");


          for (Object obj : names) {
            // Append a row
            boundariesModel.addRow(new Object[] {(String) obj});
          }


        }
      }
    });
    GroupLayout gl_panel_3 = new GroupLayout(panel_3);
    gl_panel_3.setHorizontalGroup(
      gl_panel_3.createParallelGroup(Alignment.TRAILING)
        .addGroup(gl_panel_3.createSequentialGroup()
          .addContainerGap()
          .addGroup(gl_panel_3.createParallelGroup(Alignment.LEADING)
            .addComponent(scrollPane_1, GroupLayout.DEFAULT_SIZE, 178, Short.MAX_VALUE)
            .addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 178, Short.MAX_VALUE)
            .addComponent(button, GroupLayout.DEFAULT_SIZE, 178, Short.MAX_VALUE)
            .addComponent(btn_addTempereture, GroupLayout.DEFAULT_SIZE, 178, Short.MAX_VALUE)
            .addComponent(button_2, GroupLayout.DEFAULT_SIZE, 178, Short.MAX_VALUE)
            .addGroup(gl_panel_3.createSequentialGroup()
              .addComponent(lblNewLabel_3)
              .addPreferredGap(ComponentPlacement.RELATED, 39, Short.MAX_VALUE)
              .addComponent(btnDeleteLoad))
            .addGroup(gl_panel_3.createSequentialGroup()
              .addComponent(btnGetboundaries, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
              .addPreferredGap(ComponentPlacement.RELATED)
              .addComponent(btnGb)))
          .addContainerGap())
    );
    gl_panel_3.setVerticalGroup(
      gl_panel_3.createParallelGroup(Alignment.LEADING)
        .addGroup(gl_panel_3.createSequentialGroup()
          .addContainerGap()
          .addGroup(gl_panel_3.createParallelGroup(Alignment.BASELINE)
            .addComponent(btnGetboundaries)
            .addComponent(btnGb))
          .addPreferredGap(ComponentPlacement.RELATED)
          .addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 134, GroupLayout.PREFERRED_SIZE)
          .addPreferredGap(ComponentPlacement.RELATED)
          .addComponent(button)
          .addPreferredGap(ComponentPlacement.RELATED)
          .addComponent(btn_addTempereture)
          .addPreferredGap(ComponentPlacement.RELATED)
          .addComponent(button_2)
          .addPreferredGap(ComponentPlacement.UNRELATED)
          .addGroup(gl_panel_3.createParallelGroup(Alignment.BASELINE)
            .addComponent(lblNewLabel_3)
            .addComponent(btnDeleteLoad))
          .addPreferredGap(ComponentPlacement.RELATED)
          .addComponent(scrollPane_1, GroupLayout.DEFAULT_SIZE, 54, Short.MAX_VALUE))
    );

    table_loads = new JTable(loadsModel);
    scrollPane_1.setViewportView(table_loads);

    tbl_boundaries = new JTable(boundariesModel);
    tbl_boundaries.setOpaque(false);
    tbl_boundaries.setRowSelectionAllowed(false);
    tbl_boundaries.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseReleased(MouseEvent arg0) {


        if(thermalDynamic!=null)
        thermalDynamic.setSelectedPlane((String) tbl_boundaries.getValueAt(
            tbl_boundaries.getSelectedRow(), tbl_boundaries.getSelectedColumn()));
        
        //TODO удалить изменить улучьшить
        if(staticStranalysis!=null)
          staticStranalysis.setSelectedPlane((String) tbl_boundaries.getValueAt(
          tbl_boundaries.getSelectedRow(), tbl_boundaries.getSelectedColumn()));
      
      }
    });

    scrollPane.setViewportView(tbl_boundaries);
    panel_3.setLayout(gl_panel_3);

    JPanel panel = new JPanel();
    tabbedPane.addTab("Материал", null, panel, null);

    JLabel label_2 = new JLabel("Материал");

    JButton btnNewButton = new JButton("Выбрать из БД");

    JLabel label_3 = new JLabel("Модуль упругости");

    JLabel label_4 = new JLabel("Коэффициента пуфссона");

    JLabel label_5 = new JLabel("Плотность");

    JLabel label_6 = new JLabel("Коэффициент пеплопроводности л");

    JLabel label_7 = new JLabel("Коэффициента теплоемкости");

    textField = new JTextField();
    textField.setText("200000000000");
    textField.setColumns(10);

    textField_1 = new JTextField();
    textField_1.setText("0.3");
    textField_1.setColumns(10);

    textField_2 = new JTextField();
    textField_2.setText("7850");
    textField_2.setColumns(10);

    textField_3 = new JTextField();
    textField_3.setText("60.5");
    textField_3.setColumns(10);

    textField_4 = new JTextField();
    textField_4.setText("434");
    textField_4.setColumns(10);
    
    JLabel label_8 = new JLabel("Коэффициент линейного расширения");
    
    tb_alp = new JTextField();
    tb_alp.setText("13");
    tb_alp.setColumns(10);
    GroupLayout gl_panel = new GroupLayout(panel);
    gl_panel.setHorizontalGroup(
      gl_panel.createParallelGroup(Alignment.LEADING)
        .addGroup(gl_panel.createSequentialGroup()
          .addContainerGap()
          .addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
            .addComponent(textField, GroupLayout.DEFAULT_SIZE, 201, Short.MAX_VALUE)
            .addComponent(textField_1, GroupLayout.DEFAULT_SIZE, 201, Short.MAX_VALUE)
            .addGroup(gl_panel.createSequentialGroup()
              .addComponent(label_2)
              .addPreferredGap(ComponentPlacement.UNRELATED)
              .addComponent(btnNewButton, GroupLayout.DEFAULT_SIZE, 141, Short.MAX_VALUE))
            .addComponent(label_3)
            .addComponent(label_4)
            .addComponent(label_5)
            .addComponent(textField_2, GroupLayout.DEFAULT_SIZE, 201, Short.MAX_VALUE)
            .addComponent(label_6, GroupLayout.DEFAULT_SIZE, 201, Short.MAX_VALUE)
            .addComponent(textField_3, GroupLayout.DEFAULT_SIZE, 201, Short.MAX_VALUE)
            .addComponent(label_7)
            .addComponent(textField_4, GroupLayout.DEFAULT_SIZE, 201, Short.MAX_VALUE)
            .addComponent(label_8)
            .addComponent(tb_alp, GroupLayout.DEFAULT_SIZE, 201, Short.MAX_VALUE))
          .addContainerGap())
    );
    gl_panel.setVerticalGroup(
      gl_panel.createParallelGroup(Alignment.LEADING)
        .addGroup(gl_panel.createSequentialGroup()
          .addContainerGap()
          .addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
            .addComponent(label_2)
            .addComponent(btnNewButton))
          .addGap(18)
          .addComponent(label_3)
          .addPreferredGap(ComponentPlacement.RELATED)
          .addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          .addGap(18)
          .addComponent(label_4)
          .addPreferredGap(ComponentPlacement.RELATED)
          .addComponent(textField_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          .addGap(18)
          .addComponent(label_5)
          .addPreferredGap(ComponentPlacement.RELATED)
          .addComponent(textField_2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          .addGap(18)
          .addComponent(label_6)
          .addPreferredGap(ComponentPlacement.RELATED)
          .addComponent(textField_3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          .addGap(18)
          .addComponent(label_7)
          .addPreferredGap(ComponentPlacement.RELATED)
          .addComponent(textField_4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          .addPreferredGap(ComponentPlacement.UNRELATED)
          .addComponent(label_8)
          .addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
          .addComponent(tb_alp, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          .addContainerGap())
    );
    panel.setLayout(gl_panel);

    JPanel panel_6 = new JPanel();
    tabbedPane.addTab("Решение", null, panel_6, null);

    JLabel label_1 = new JLabel("Параметры расчета");

    JLabel lblNewLabel_4 = new JLabel("Начальная температура");

    tb_intitialTemperature = new JTextField();
    tb_intitialTemperature.setText("300");
    tb_intitialTemperature.setColumns(10);

    JButton btn_solve = new JButton("Расчет");
    btn_solve.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent arg0) {


        // TODO добавить валидацию

        // Считывание начальных данных исследования
        double initialThemperature = Double.parseDouble(tb_intitialTemperature.getText());
        double timeStudy = Double.parseDouble(tb_timeStudy.getText());
        double timeStep = Double.parseDouble(tb_timeStep.getText());
        DataInitThermalDynamic initData =
            new DataInitThermalDynamic(timeStudy, timeStep, initialThemperature);
        thermalDynamic.setDataInit(initData);

        // double c = ReadDataFromWND.readDoubleFromTB(tb_c);
        // double lambda = ReadDataFromWND.readDoubleFromTB(tb_lambda);
        // double ro = ReadDataFromWND.readDoubleFromTB(tb_ro);
        // double E = ReadDataFromWND.readDoubleFromTB(tb_e);
        // double my = ReadDataFromWND.readDoubleFromTB(tb_my);

        // Material matherial = new Material(E, my, ro, lambda, c);

        // TextBox[] tb = { tb_loadB, tb_n2, tb_n3, tb_n4, tb_n5 };
        // List<Int32> num = new List<int>();

        Material material = new Material();

        material.setDensity(7850);
        material.setElasticModulus(200000000000f);
        material.setName("Сталь");
        material.setPoissonsRatio(0.3);
        material.setSpecificHeatCapacity(434);
        material.setThermalConductivity(60.5);

        thermalDynamic.getMesh().getMaterial().put(0, material);
        // TODO останавливать прорисовку при запуске расчета

        double endTime = 500;// ReadDataFromWND.readDoubleFromTB(tb_endTime);
        double stepTime = 10;// ReadDataFromWND.readDoubleFromTB(tb_stepTime);

        ThermalDynamicSolver thermalDynamicSolver = new ThermalDynamicSolver(thermalDynamic);

        try {
          thermalDynamicSolver.formAllGlovalMatrix();
        } catch (Exception e) {
          e.printStackTrace();
        }

        // List<int> dnn =
        // mesh.bondary.left.Concat(mesh.bondary.right).Concat(mesh.bondary.front).Concat(mesh.bondary.back).ToList<int>();
        // List<int> dnn = mesh.bondary.front.Concat(mesh.bondary.back).ToList<int>();



        thermalDynamicSolver.Solve();

        double maxT = thermalDynamicSolver.T[0][0];
        double minT = thermalDynamicSolver.T[0][0];

        for (int i = 0; i < thermalDynamicSolver.T.length; i++) {
          for (int j = 0; j < thermalDynamicSolver.T[0].length; j++) {
            if (Math.abs(thermalDynamicSolver.T[i][j]) > Math.abs(maxT))
              maxT = thermalDynamicSolver.T[i][j];
            if (Math.abs(thermalDynamicSolver.T[i][j]) < Math.abs(minT))
              minT = thermalDynamicSolver.T[i][j];
          }
        }

        JOptionPane.showMessageDialog(null, "Solve!\n  min =" + minT + "\n max = " + maxT,
            "Результат", JOptionPane.OK_OPTION);
        System.out.println("dsf");
        System.out.println(thermalDynamic.getMesh().getNodes().size());
        System.out.println(thermalDynamicSolver.T.length);
        System.out.println(thermalDynamicSolver.T[0].length);
        displayViewer.thermalDynamicSolver = thermalDynamicSolver;
        displayViewer.setTypeDisplay(DisplayType.RESULT);

      }
    });

    JLabel lblNewLabel_5 = new JLabel("Шаг по времени, с");

    tb_timeStep = new JTextField();
    tb_timeStep.setText("10");
    tb_timeStep.setColumns(10);

    JLabel lblNewLabel_6 = new JLabel("Время исследования, с");

    tb_timeStudy = new JTextField();
    tb_timeStudy.setText("300");
    tb_timeStudy.setColumns(10);
    
    JButton button_3 = new JButton("Расчет Механики");
    button_3.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
    
        
        
        //Предварительный расчет тепла
     // TODO добавить валидацию

        // Считывание начальных данных исследования
        double initialThemperature = Double.parseDouble(tb_intitialTemperature.getText());
        double timeStudy = Double.parseDouble(tb_timeStudy.getText());
        double timeStep = Double.parseDouble(tb_timeStep.getText());
        DataInitThermalDynamic initData =
            new DataInitThermalDynamic(timeStudy, timeStep, initialThemperature);
        thermalDynamic.setDataInit(initData);

        // double c = ReadDataFromWND.readDoubleFromTB(tb_c);
        // double lambda = ReadDataFromWND.readDoubleFromTB(tb_lambda);
        // double ro = ReadDataFromWND.readDoubleFromTB(tb_ro);
        // double E = ReadDataFromWND.readDoubleFromTB(tb_e);
        // double my = ReadDataFromWND.readDoubleFromTB(tb_my);

        // Material matherial = new Material(E, my, ro, lambda, c);

        // TextBox[] tb = { tb_loadB, tb_n2, tb_n3, tb_n4, tb_n5 };
        // List<Int32> num = new List<int>();

        Material material = new Material();

        material.setDensity(7850);
        material.setElasticModulus(200000000000f);
        material.setName("Сталь");
        material.setPoissonsRatio(0.3);
        material.setSpecificHeatCapacity(434);
        material.setThermalConductivity(60.5);

        thermalDynamic.getMesh().getMaterial().put(0, material);
        // TODO останавливать прорисовку при запуске расчета

        
        ThermalDynamicSolver thermalDynamicSolver = new ThermalDynamicSolver(thermalDynamic);

        try {
          thermalDynamicSolver.formAllGlovalMatrix();
        } catch (Exception ex) {
          ex.printStackTrace();
        }

        // List<int> dnn =
        // mesh.bondary.left.Concat(mesh.bondary.right).Concat(mesh.bondary.front).Concat(mesh.bondary.back).ToList<int>();
        // List<int> dnn = mesh.bondary.front.Concat(mesh.bondary.back).ToList<int>();



        thermalDynamicSolver.Solve();
        
        //Предварительный расчет тепла
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        // TODO добавить валидацию

        // Считывание начальных данных исследования
       // double initialThemperature = Double.parseDouble(tb_intitialTemperature.getText());
        //double timeStudy = Double.parseDouble(tb_timeStudy.getText());
        //double timeStep = Double.parseDouble(tb_timeStep.getText());
        //DataInitThermalDynamic initData =
          //  new DataInitThermalDynamic(timeStudy, timeStep, initialThemperature);
        //thermalDynamic.setDataInit(initData);

        // double c = ReadDataFromWND.readDoubleFromTB(tb_c);
        // double lambda = ReadDataFromWND.readDoubleFromTB(tb_lambda);
        // double ro = ReadDataFromWND.readDoubleFromTB(tb_ro);
        // double E = ReadDataFromWND.readDoubleFromTB(tb_e);
        // double my = ReadDataFromWND.readDoubleFromTB(tb_my);

        // Material matherial = new Material(E, my, ro, lambda, c);

        // TextBox[] tb = { tb_loadB, tb_n2, tb_n3, tb_n4, tb_n5 };
        // List<Int32> num = new List<int>();

        material = new Material();

        material.setDensity(7850);
        material.setElasticModulus(200000000000f);
        material.setName("Сталь");
        material.setPoissonsRatio(0.3);
        material.setSpecificHeatCapacity(434);
        material.setThermalConductivity(60.5);
        material.setThermalExpansion(0.000012);
        
        staticStranalysis.getMesh().getMaterial().put(0, material);
        // TODO останавливать прорисовку при запуске расчета

        //double endTime = 500;// ReadDataFromWND.readDoubleFromTB(tb_endTime);
        //double stepTime = 10;// ReadDataFromWND.readDoubleFromTB(tb_stepTime);
        double[] temper = new double[ thermalDynamicSolver.T.length];
        for(int i =0;i<thermalDynamicSolver.T.length;i++) {
          temper[i] = thermalDynamicSolver.T[i][thermalDynamicSolver.T[0].length-1];
        }
        
        staticStranalysis.setDataInit(new DataInitStaticStructal(temper));
        StaticStructalSolver staticStructalSolver = new StaticStructalSolver(staticStranalysis);
        
        // List<int> dnn =
        // mesh.bondary.left.Concat(mesh.bondary.right).Concat(mesh.bondary.front).Concat(mesh.bondary.back).ToList<int>();
        // List<int> dnn = mesh.bondary.front.Concat(mesh.bondary.back).ToList<int>();

        

        staticStructalSolver.Solve();

        double max = staticStructalSolver.getResultX(0);
       
        for (int i = 0; i < staticStranalysis.getMesh().getNodes().size(); i++) {
          
          if (Math.abs(staticStructalSolver.getResultX(i)) > Math.abs(max))
            max = staticStructalSolver.getResultX(i);
          if (Math.abs(staticStructalSolver.getResultY(i)) > Math.abs(max))
            max = staticStructalSolver.getResultY(i);
          if (Math.abs(staticStructalSolver.getResultZ(i)) > Math.abs(max))
            max = staticStructalSolver.getResultZ(i);
        
        }

        JOptionPane.showMessageDialog(null, "Solve!\n  max ="+ max,
            "Результат", JOptionPane.OK_OPTION);
        
        displayViewer.structSolver = staticStructalSolver;
        displayViewer.setTypeDisplay(DisplayType.MESHRESULT);
        
        
        
        
      }
    });
    GroupLayout gl_panel_6 = new GroupLayout(panel_6);
    gl_panel_6.setHorizontalGroup(
      gl_panel_6.createParallelGroup(Alignment.LEADING)
        .addGroup(gl_panel_6.createSequentialGroup()
          .addContainerGap()
          .addGroup(gl_panel_6.createParallelGroup(Alignment.LEADING)
            .addComponent(tb_intitialTemperature, GroupLayout.DEFAULT_SIZE, 178, Short.MAX_VALUE)
            .addComponent(label_1)
            .addComponent(lblNewLabel_4)
            .addComponent(lblNewLabel_5)
            .addComponent(tb_timeStep, GroupLayout.DEFAULT_SIZE, 178, Short.MAX_VALUE)
            .addComponent(lblNewLabel_6)
            .addComponent(tb_timeStudy, GroupLayout.DEFAULT_SIZE, 178, Short.MAX_VALUE)
            .addComponent(btn_solve, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 178, Short.MAX_VALUE)
            .addComponent(button_3, GroupLayout.DEFAULT_SIZE, 178, Short.MAX_VALUE))
          .addContainerGap())
    );
    gl_panel_6.setVerticalGroup(
      gl_panel_6.createParallelGroup(Alignment.LEADING)
        .addGroup(gl_panel_6.createSequentialGroup()
          .addContainerGap()
          .addComponent(label_1)
          .addPreferredGap(ComponentPlacement.UNRELATED)
          .addComponent(lblNewLabel_4)
          .addPreferredGap(ComponentPlacement.RELATED)
          .addComponent(tb_intitialTemperature, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          .addGap(32)
          .addComponent(lblNewLabel_5)
          .addPreferredGap(ComponentPlacement.RELATED)
          .addComponent(tb_timeStep, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          .addGap(18)
          .addComponent(lblNewLabel_6)
          .addPreferredGap(ComponentPlacement.RELATED)
          .addComponent(tb_timeStudy, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          .addGap(18)
          .addComponent(btn_solve)
          .addGap(38)
          .addComponent(button_3)
          .addContainerGap(47, Short.MAX_VALUE))
    );
    panel_6.setLayout(gl_panel_6);

    JSplitPane splitPane_1 = new JSplitPane();
    splitPane_1.setOrientation(JSplitPane.VERTICAL_SPLIT);
    splitPane.setRightComponent(splitPane_1);

    JPanel panel_4 = new JPanel();
    panel_4.setPreferredSize(new Dimension(10, 25));
    panel_4.setMinimumSize(new Dimension(10, 6));
    panel_4.setBorder(null);
    splitPane_1.setLeftComponent(panel_4);

    final JButton btn_toogleAnimate = new JButton("Вкл. анимацию");
    btn_toogleAnimate.setPreferredSize(new Dimension(109, 20));
    btn_toogleAnimate.setForeground(new Color(51, 51, 51));
    btn_toogleAnimate.setFont(new Font("Verdana", Font.BOLD, 12));
    setupDialogButtonUI(btn_toogleAnimate, 0);
    btn_toogleAnimate.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseReleased(MouseEvent arg0) {

        if (animator.isStarted()) {
          animator.stop();
          btn_toogleAnimate.setText("Вкл. Анимацию");
        } else {
          animator.start();
          btn_toogleAnimate.setText("Выкл. Анимацию");
        }
      }
    });

    JButton btnNewButton_1 = new JButton("full screen beta");
    btnNewButton_1.setEnabled(false);
    btnNewButton_1.addActionListener(new FullScreenEffect());

    JButton btn_lastTime = new JButton("<");
    btn_lastTime.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent arg0) {
        ((IDynamicResult)thermalDynamic.getResult()).previousTimeLayer();
      }
    });
    btn_lastTime.setToolTipText("Предыдущий момент времени");

    JLabel lb_timeNow = new JLabel("0");
    //TODO добавить пересчет диопозонов температуры для кравивой картинки
    JButton btn_nextTime = new JButton(">");
    btn_nextTime.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent arg0) {

        ((IDynamicResult)thermalDynamic.getResult()).nextTimeLayer();
      }
    });
    
    JButton btnNewButton_2 = new JButton("New button");
    
    JButton btn_drawInTime = new JButton("Просмотр во времени");
    btn_drawInTime.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent arg0) {
        if(timer==null) {
        timer = new Timer(true);
        
        TimerTask timerTask = new TimerTask() {
          
          @Override
          public void run() {
            ((IDynamicResult)thermalDynamic.getResult()).nextTimeLayer();
          }
        };
        
        timer.schedule(timerTask, 0, 100);
        }
        else {
          timer.cancel();
          timer = null;
        }
      }
    });
    
    GroupLayout gl_panel_4 = new GroupLayout(panel_4);
    gl_panel_4.setHorizontalGroup(
      gl_panel_4.createParallelGroup(Alignment.TRAILING)
        .addGroup(gl_panel_4.createSequentialGroup()
          .addComponent(btn_toogleAnimate, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          .addPreferredGap(ComponentPlacement.RELATED)
          .addComponent(btnNewButton_1)
          .addPreferredGap(ComponentPlacement.RELATED)
          .addComponent(btn_lastTime)
          .addPreferredGap(ComponentPlacement.RELATED)
          .addComponent(lb_timeNow, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
          .addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
          .addGroup(gl_panel_4.createParallelGroup(Alignment.LEADING)
            .addComponent(btnNewButton_2)
            .addGroup(gl_panel_4.createSequentialGroup()
              .addComponent(btn_nextTime)
              .addPreferredGap(ComponentPlacement.RELATED)
              .addComponent(btn_drawInTime)))
          .addContainerGap())
    );
    gl_panel_4.setVerticalGroup(
      gl_panel_4.createParallelGroup(Alignment.LEADING)
        .addGroup(gl_panel_4.createSequentialGroup()
          .addGroup(gl_panel_4.createParallelGroup(Alignment.BASELINE)
            .addComponent(btn_toogleAnimate, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addComponent(btnNewButton_1)
            .addComponent(btn_lastTime)
            .addComponent(lb_timeNow)
            .addComponent(btn_nextTime)
            .addComponent(btn_drawInTime))
          .addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
          .addComponent(btnNewButton_2))
    );
    panel_4.setLayout(gl_panel_4);

    JPanel panel_5 = new JPanel();
    splitPane_1.setRightComponent(panel_5);
    panel_5.setLayout(new BorderLayout(0, 0));
    panel_5.add(displayViewer);
    displayViewer.setRealized(true);

    // TODO delete
    displayViewer.setMinimumSize(new DimensionUIResource(50, 50));
    displayViewer.setMaximumSize(new DimensionUIResource(400, 400));
    displayViewer.setSize(new DimensionUIResource(200, 200));



    displayViewer.addMouseMotionListener(new MouseMotionAdapter() {
      @Override
      public void mouseDragged(MouseEvent arg0) {

        if (arg0.isControlDown()) {
          // lb_testX.setText(Double.toString(arg0.getXOnScreen()));
          // lb_testY.setText(Double.toString(arg0.getYOnScreen()));
          double currentX = arg0.getXOnScreen();
          double currentY = arg0.getYOnScreen();

          double oy = Math.abs(currentY - lastY);
          double ox = Math.abs(currentX - lastX);

          if (oy < ox) {
            if (lastX < currentX) {
              displayViewer.addToMove_x(0.05);
            } else {
              displayViewer.addToMove_x(-0.05);
            }

          } else {
            if (lastY < currentY) {
              displayViewer.addToMove_y(-0.05);
            } else {
              displayViewer.addToMove_y(0.05);
            }
          }
          lastX = currentX;
          lastY = currentY;

        } else {
          displayViewer.setCamera_angle_x(arg0.getX());
          displayViewer.setCamera_angle_y(arg0.getY());
        }
      }
    });

    displayViewer.addMouseWheelListener(mouseWhellListener);
  }



  static void renderSplashFrame(Graphics2D g, int frame) {
    final String[] comps = {"Модели", "Графика", "Решатели"};
    g.setComposite(AlphaComposite.Clear);
    g.fillRect(350, 180, 200, 40);

    g.setPaintMode();
    g.setColor(Color.BLACK);
    g.drawString("Loading " + comps[(frame / 5) % 3] + "...", 350, 192);

  }


  @Override
  public void actionPerformed(ActionEvent e) {

    System.exit(0);
  }
}
