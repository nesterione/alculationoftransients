package by.nesterenya.glviewers;

import static javax.media.opengl.GL.*;
import static javax.media.opengl.GL2ES1.GL_PERSPECTIVE_CORRECTION_HINT;
import static javax.media.opengl.fixedfunc.GLLightingFunc.GL_SMOOTH;
import static javax.media.opengl.fixedfunc.GLMatrixFunc.GL_PROJECTION;

import java.nio.FloatBuffer;
import java.util.List;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GL2ES1;
import javax.media.opengl.GL2GL3;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.awt.GLCanvas;
import javax.media.opengl.fixedfunc.GLLightingFunc;
import javax.media.opengl.fixedfunc.GLMatrixFunc;
import javax.media.opengl.glu.GLU;

import by.nesterenya.analysis.Analysis;
import by.nesterenya.analysis.IDynamicResult;
import by.nesterenya.analysis.StaticStructalAnalisis;
import by.nesterenya.analysis.ThermalDynamicAnalisis;
import by.nesterenya.boundary.EvenlyDistributedLoad;
import by.nesterenya.boundary.ILoad;
import by.nesterenya.boundary.StaticTemperature;
import by.nesterenya.boundary.Support;
import by.nesterenya.elements.IElement;
import by.nesterenya.elements.INode;
import by.nesterenya.elements.INode.Dim;
import by.nesterenya.solvers.StaticStructalSolver;
import by.nesterenya.solvers.ThermalDynamicSolver;

public class GlViewer extends GLCanvas implements GLEventListener {

  public enum DisplayType {
    MODEL, MESH, RESULT, MESHRESULT
  };

  private static final long serialVersionUID = 1L;

  double cK = 1;
  public void setCorrectKaf(double correctKaf) {
    cK = correctKaf;
  }
  
  @Override
  public void dispose(GLAutoDrawable drawable) {}

  @Override
  public void display(GLAutoDrawable drawable) {

    if (analysis == null) return;
    if (displayType == null) return;

    gl = drawable.getGL().getGL2();

    // clear
    gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);

    // Сброс параметов модели отображения
    gl.glLoadIdentity();
    gl.glTranslatef(0.0f, 0.0f, -30.0f);

    // фон
    gl.glBegin(GL2GL3.GL_QUADS);
    // white color
    gl.glColor3f(0.42f, 0.55f, 0.83f);
    gl.glVertex2f(30.0f, 20.0f);
    gl.glVertex2f(-30.0f, 20.0f);
    // blue color
    gl.glColor3f(1.0f, 1.0f, 1.0f);
    gl.glVertex2f(-30.0f, -20.0f);
    gl.glVertex2f(30.0f, -20.0f);
    gl.glEnd();

    // Сместить в нуть на 30
    gl.glTranslatef(0.0f, 0.0f, 30.0f);

    try {
      switch (displayType) {
        case MODEL:
          plotModel();
          break;
        case MESH:
          plotMesh();
          break;
        case RESULT:
          plotThermalResult();
          break;
        case MESHRESULT:
          plotMehResult();
          break;
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  // TODO Сделать боллее обобщенную модель для того что бы подходила для любых типов задач
  //private ThermalDynamicAnalisis thermalDynamic;
  private Analysis analysis;
  private double zoom = 1.0;
  private DisplayType displayType;
  private double camera_angle_x = 0;
  private double camera_angle_y = 0;
  private double move_x = 0;
  private double move_y = 0;
  // Для дополнительных GL возможностей
  private GLU glu;
  private GL2 gl;
  public ThermalDynamicSolver thermalDynamicSolver = null;


  public double getZoom() {
    return zoom;
  }

  public void addToZoom(double deltaZoom) {
    zoom += deltaZoom;
  }

  public void addToMove_x(double deltaX) {
    move_x += deltaX;
  }

  public void addToMove_y(double deltaY) {
    move_y += deltaY;
  }

  public void addToCamera_angle_x(double deltaX) {
    camera_angle_x += deltaX;
  }

  public void addToCamera_angle_y(double deltaY) {
    camera_angle_y += deltaY;
  }

  public void setZoom(double zoom) {
    this.zoom = zoom;
  }

  public double getCamera_angle_x() {
    return camera_angle_x;
  }

  public void setCamera_angle_x(double camera_angle_x) {
    this.camera_angle_x = camera_angle_x;
  }

  public double getCamera_angle_y() {
    return camera_angle_y;
  }

  public void setCamera_angle_y(double camera_angle_y) {
    this.camera_angle_y = camera_angle_y;
  }

  public double getMove_x() {
    return move_x;
  }

  public void setMove_x(double move_x) {
    this.move_x = move_x;
  }

  public double getMove_y() {
    return move_y;
  }

  public void setMove_y(double move_y) {
    this.move_y = move_y;
  }

  public GlViewer() {
    this.addGLEventListener(this);
    //TODO убрать это от сюда
    analysis = new ThermalDynamicAnalisis();
  }

  @Override
  public void init(GLAutoDrawable drawable) {
    // Получить GL контекст
    GL2 gl = drawable.getGL().getGL2();
    // Получить GL инструменты
    glu = new GLU();
    // Цвет фона
    gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    // Установить очистку буфера глубины
    gl.glClearDepth(1.0f);
    gl.glEnable(GL_DEPTH_TEST);
    gl.glDepthFunc(GL_LEQUAL);
    gl.glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // лучшая
                                                          // настройка
                                                          // перспективы
    gl.glShadeModel(GL_SMOOTH); // вклиютить смешение цветов, размытие
                                // и освещение
  }

  public void setTypeDisplay(DisplayType displayType) {
    this.displayType = displayType;
  }

  @Override
  public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
    // Получить контекст OpenGL 2
    GL2 gl = drawable.getGL().getGL2();
    // Проверка деления на ноль
    if (height == 0) height = 1;
    float aspect = (float) width / height;
    // Установить окна отбражения
    gl.glViewport(0, 0, width, height);

    // Установить перспективную проекцию
    // Выбор матрицы проекций
    gl.glMatrixMode(GL_PROJECTION);

    // Сбросить матрицу проекций
    gl.glLoadIdentity();

    // fovy, aspect, zNear, zFar
    glu.gluPerspective(45.0, aspect, 0.1, 100.0);
    // Включить model-view перемещения
    gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW);
    // Сбросить матрицу проекций
    gl.glLoadIdentity();
  }

  public void setThermalDynamic(ThermalDynamicAnalisis thermalDynamic) {
    this.analysis = thermalDynamic;
  }

  public void setStructalAnalysis(StaticStructalAnalisis structalAnalysis) {
    this.analysis = structalAnalysis;
  }
  
  private void plotModel() {

    gl.glTranslatef(0.0f, 0.0f, -6.0f);
    gl.glScaled(zoom, zoom, zoom);

    // TODO изменить положение камеры правильным образом
    // gluLookAt(0.0, 0.0, 25.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);

    gl.glRotated(camera_angle_x, 0.0, 1.0, 0.0);
    gl.glRotated(camera_angle_y, 1.0, 0.0, 0.0);

    // Рисуем координатные оси
    // TODO перейменовать
    GlPrimitives.drawOsi(gl);

    gl.glTranslated(move_x, move_y, 0);

    gl.glPushMatrix();

    // Материал серебро
    float ambient[] = {0.0215f, 0.1745f, 0.0215f, 1.0f};
    float diffuse[] = {0.07568f, 0.61424f, 0.07568f, 1.0f};
    float specular[] = {0.508273f, 0.508273f, 0.508273f, 1.0f};
    float shine = 0.4f;

    gl.glMaterialfv(GL.GL_FRONT, GLLightingFunc.GL_AMBIENT, FloatBuffer.wrap(ambient));
    gl.glMaterialfv(GL.GL_FRONT, GLLightingFunc.GL_DIFFUSE, FloatBuffer.wrap(diffuse));
    gl.glMaterialfv(GL.GL_FRONT, GLLightingFunc.GL_SPECULAR, FloatBuffer.wrap(specular));
    gl.glMaterialf(GL.GL_FRONT, GLLightingFunc.GL_SHININESS, shine * 128.0f);

    gl.glColor3f(0.83f, 0.83f, 0.83f);

    // TODO Можно добавить обстрактную фабрику для создания метода геометрии
    GlPrimitives.drawBox(gl, analysis.getGeometry());

    gl.glColor3f(0.3f, 0.3f, 0.3f);
    gl.glPolygonMode(GL.GL_FRONT_AND_BACK, GL2GL3.GL_LINE);
    GlPrimitives.drawBox(gl, analysis.getGeometry());
    gl.glPolygonMode(GL.GL_FRONT_AND_BACK, GL2GL3.GL_FILL);

    gl.glPopMatrix();

    // Ждать завершения прорисовки
    gl.glFlush();
  }

  private void plotMesh() throws Exception {

    gl.glTranslatef(0.0f, 0.0f, -6.0f);
    gl.glScaled(zoom, zoom, zoom); // screen

    // TODO изменить положение камеры правильным образом
    // gluLookAt(0.0, 0.0, 25.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);

    gl.glRotated(camera_angle_x, 0.0, 1.0, 0.0);
    gl.glRotated(camera_angle_y, 1.0, 0.0, 0.0);

    // Рисуем координатные оси
    GlPrimitives.drawOsi(gl);

    gl.glTranslated(move_x, move_y, 0);

    gl.glEnable(GL2ES1.GL_ALPHA_TEST);
    gl.glEnable(GL.GL_BLEND);
    gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);

    gl.glColor4f(1.0f, 0.0f, 0.0f, 1.0f);

    gl.glEnable(GL2ES1.GL_POINT_SMOOTH); // включаем режим сглаживания точек
    gl.glPointSize(4);
    gl.glBegin(GL.GL_POINTS);


    for (INode node : analysis.getMesh().getNodes()) {
      drawGlVertex3d(node);
    }

    gl.glEnd();

    // Включить отрисовку линий, цвет линий синий
    gl.glColor4f(0.0f, 0.0f, 1.0f, 1.0f);
    gl.glPolygonMode(GL.GL_FRONT_AND_BACK, GL2GL3.GL_LINE);
    // Смещение, для того чтобы лиини были лучше видны
    gl.glTranslatef(0, 0, 0.001f);

    // Отрисовываем все конечные элементы
    gl.glBegin(GL.GL_TRIANGLES);
    List<IElement> elements = analysis.getMesh().getElements();
    for (IElement element : elements) {
      INode node0 = element.getNode(0);
      INode node1 = element.getNode(1);
      INode node2 = element.getNode(2);
      INode node3 = element.getNode(3);

      drawGlVertex3d(node0);
      drawGlVertex3d(node1);
      drawGlVertex3d(node2);

      drawGlVertex3d(node0);
      drawGlVertex3d(node1);
      drawGlVertex3d(node3);

      drawGlVertex3d(node1);
      drawGlVertex3d(node2);
      drawGlVertex3d(node3);

      drawGlVertex3d(node0);
      drawGlVertex3d(node2);
      drawGlVertex3d(node3);
    }
    gl.glEnd();

    // Выключить отображение полигонов в виде линий
    gl.glPolygonMode(GL.GL_FRONT_AND_BACK, GL2GL3.GL_FILL);
    // Сместить в исходное положение
    gl.glTranslatef(0, 0, -0.001f);

    gl.glColor4d(0.85, 0.85, 0.85, 0.45f);
    GlPrimitives.drawBox(gl, analysis.getGeometry());

    drawLoads();
    drawSelectedPlate();

    gl.glDisable(GL.GL_BLEND);
    gl.glDisable(GL2ES1.GL_ALPHA_TEST);

    gl.glFlush();
  }

  public StaticStructalSolver structSolver;
  //TODO оптимизировать
  private void plotMehResult() throws Exception {

    gl.glTranslatef(0.0f, 0.0f, -6.0f);

    gl.glScaled(zoom, zoom, zoom); // screen

    // TODO изменить положение камеры правильным образом
    // gluLookAt(0.0, 0.0, 25.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);

    gl.glRotated(camera_angle_x, 0.0, 1.0, 0.0);
    gl.glRotated(camera_angle_y, 1.0, 0.0, 0.0);

    // Рисуем координатные оси
    GlPrimitives.drawOsi(gl);

    gl.glTranslated(move_x, move_y, 0);

    gl.glEnable(GL2ES1.GL_ALPHA_TEST);
    gl.glEnable(GL.GL_BLEND);
    gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);


    gl.glTranslatef(0, 0, 0.001f);
    gl.glColor4f(1.0f, 0.0f, 0.0f, 1.0f);

    gl.glPointSize(4);
    gl.glEnable(GL2ES1.GL_POINT_SMOOTH); // включаем режим сглаживания точек

    //gl.glBegin(GL.GL_POINTS);

    //for (INode node : analysis.getMesh().getNodes()) {
    //  drawGlVertex3d(node);
   // }

    gl.glEnd();

    gl.glColor4f(0.0f, 0.0f, 1.0f, 1.0f);
    gl.glPolygonMode(GL.GL_FRONT_AND_BACK, GL2GL3.GL_FILL);

    gl.glBegin(GL.GL_TRIANGLES);
    List<IElement> elements = analysis.getMesh().getElements();
    for (IElement element : elements) {


      INode node0 = element.getNode(0);
      INode node1 = element.getNode(1);
      INode node2 = element.getNode(2);
      INode node3 = element.getNode(3);

     
      DrawGLColor3fStruct(node0);
      drawGlVertex3d_stress(node0);
      DrawGLColor3fStruct(node1);
      drawGlVertex3d_stress(node1);
      DrawGLColor3fStruct(node2);
      drawGlVertex3d_stress(node2);

      DrawGLColor3fStruct(node0);
      drawGlVertex3d_stress(node0);
      DrawGLColor3fStruct(node1);
      drawGlVertex3d_stress(node1);
      DrawGLColor3fStruct(node3);
      drawGlVertex3d_stress(node3);

      DrawGLColor3fStruct(node1);
      drawGlVertex3d_stress(node1);
      DrawGLColor3fStruct(node2);
      drawGlVertex3d_stress(node2);  
      DrawGLColor3fStruct(node3);
      drawGlVertex3d_stress(node3);

      DrawGLColor3fStruct(node0);
      drawGlVertex3d_stress(node0);  
      DrawGLColor3fStruct(node2);
      drawGlVertex3d_stress(node2);
      DrawGLColor3fStruct(node3);
      drawGlVertex3d_stress(node3);
    }
    gl.glEnd();

    gl.glPolygonMode(GL.GL_FRONT_AND_BACK, GL2GL3.GL_FILL);
    gl.glTranslatef(0, 0, -0.001f);

    gl.glDisable(GL.GL_BLEND);
    gl.glDisable(GL2ES1.GL_ALPHA_TEST);
    gl.glFlush();
  }



  // TODO Может быть переместить в лучшее место, может методы расширения
  private void drawGlVertex3d_stress(INode node) throws Exception {
    
    int numberNode = analysis.getMesh().getNodes().lastIndexOf(node);
    
    gl.glVertex3d(node.getValueOfDemention(Dim.X)+structSolver.getResultX(numberNode)*cK, 
      node.getValueOfDemention(Dim.Y)+structSolver.getResultY(numberNode)*cK,
        node.getValueOfDemention(Dim.Z)+structSolver.getResultZ(numberNode)*cK);
  }
  
  
  
  
  
  
  private void plotThermalResult() throws Exception {

    gl.glTranslatef(0.0f, 0.0f, -6.0f);

    gl.glScaled(zoom, zoom, zoom); // screen

    // TODO изменить положение камеры правильным образом
    // gluLookAt(0.0, 0.0, 25.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);

    gl.glRotated(camera_angle_x, 0.0, 1.0, 0.0);
    gl.glRotated(camera_angle_y, 1.0, 0.0, 0.0);

    // Рисуем координатные оси
    GlPrimitives.drawOsi(gl);

    gl.glTranslated(move_x, move_y, 0);

    gl.glEnable(GL2ES1.GL_ALPHA_TEST);
    gl.glEnable(GL.GL_BLEND);
    gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);


    gl.glTranslatef(0, 0, 0.001f);
    gl.glColor4f(1.0f, 0.0f, 0.0f, 1.0f);

    gl.glPointSize(4);
    gl.glEnable(GL2ES1.GL_POINT_SMOOTH); // включаем режим сглаживания точек

    gl.glBegin(GL.GL_POINTS);

    for (INode node : analysis.getMesh().getNodes()) {
      drawGlVertex3d(node);
    }

    gl.glEnd();

    gl.glColor4f(0.0f, 0.0f, 1.0f, 1.0f);
    gl.glPolygonMode(GL.GL_FRONT_AND_BACK, GL2GL3.GL_FILL);

    gl.glBegin(GL.GL_TRIANGLES);
    List<IElement> elements = analysis.getMesh().getElements();
    for (IElement element : elements) {
      int timeLayer = ((IDynamicResult)analysis.getResult()).getSelectedTimeLayer();

      INode node0 = element.getNode(0);
      INode node1 = element.getNode(1);
      INode node2 = element.getNode(2);
      INode node3 = element.getNode(3);

      setColorThermal(thermalDynamicSolver.T[analysis.getMesh().getNodes().lastIndexOf(node0)][timeLayer]);

      DrawGLColor3f(node0, timeLayer);
      drawGlVertex3d(node0);
      DrawGLColor3f(node1, timeLayer);
      drawGlVertex3d(node1);
      DrawGLColor3f(node2, timeLayer);
      drawGlVertex3d(node2);

      DrawGLColor3f(node0, timeLayer);
      drawGlVertex3d(node0);
      DrawGLColor3f(node1, timeLayer);
      drawGlVertex3d(node1);
      DrawGLColor3f(node3, timeLayer);
      drawGlVertex3d(node3);

      DrawGLColor3f(node1, timeLayer);
      drawGlVertex3d(node1);
      DrawGLColor3f(node2, timeLayer);
      drawGlVertex3d(node2);
      DrawGLColor3f(node3, timeLayer);
      drawGlVertex3d(node3);

      DrawGLColor3f(node0, timeLayer);
      drawGlVertex3d(node0);
      DrawGLColor3f(node2, timeLayer);
      drawGlVertex3d(node2);
      DrawGLColor3f(node3, timeLayer);
      drawGlVertex3d(node3);
    }
    gl.glEnd();

    gl.glPolygonMode(GL.GL_FRONT_AND_BACK, GL2GL3.GL_FILL);
    gl.glTranslatef(0, 0, -0.001f);

    gl.glDisable(GL.GL_BLEND);
    gl.glDisable(GL2ES1.GL_ALPHA_TEST);
    gl.glFlush();
  }

  private void DrawGLColor3f(INode node, int timeLayer) {
    setColorThermal(thermalDynamicSolver.T[analysis.getMesh().getNodes().lastIndexOf(node)][timeLayer]);
  }
  
  private void DrawGLColor3fStruct(INode node) {
    setColorStructal(structSolver.getResultY(analysis.getMesh().getNodes().lastIndexOf(node)));
  }



  // TODO END REFACTORING

  // TODO переместить в статик класс Selections
  private void drawSelectedPlate() throws Exception {

    if (analysis.getSelectedPlane() != null
        && analysis.getSelectedPlane().trim().length() > 0) {

      gl.glColor4f(0.0f, 1.0f, 0.0f, 1.0f);

      if (analysis.getMesh().getBoundaries().containsKey(analysis.getSelectedPlane())) {

        // TODO Сделать через указатель
        GlPrimitives.drawMarkers(gl,
          analysis.getMesh().getBoundaries().get(analysis.getSelectedPlane())
                .getNodes());
      }

    }
  }


  // TODO переместить в статик класс Selections
  private void drawLoads() {
    if (analysis.getLoads() == null) return;
    for (ILoad load : analysis.getLoads()) {

      

      try {

        if (analysis.getMesh().getBoundaries().containsKey(analysis.getSelectedPlane())) {
           
          if(load instanceof StaticTemperature) {
            gl.glColor4f(1.0f, 0.4f, 0.0f, 0.8f);
          ThermalArrow.draw(gl, ((StaticTemperature) load).getBoundary().getNodes(), 0.1, 0.3,
              ((StaticTemperature) load).getBoundary().getName());
          } else 
            if(load instanceof Support){
              gl.glColor4f(0.0f, 0.0f, 0.0f, 0.8f);
              ThermalArrow.draw(gl, load.getBoundary().getNodes(), 0.1, 0.3,
                load.getBoundary().getName());
            }
            else if(load instanceof EvenlyDistributedLoad) {
              gl.glColor4f(0.0f, 0.0f, 1.0f, 0.8f);
              ThermalArrow.draw(gl, load.getBoundary().getNodes(), 0.1, 0.3,
                load.getBoundary().getName());
            }
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }



  // TODO Может быть переместить в лучшее место, может методы расширения
  private void drawGlVertex3d(INode node) throws Exception {
    gl.glVertex3d(node.getValueOfDemention(Dim.X), node.getValueOfDemention(Dim.Y),
        node.getValueOfDemention(Dim.Z));
  }

  int setColorStructal(double value) {
    //TODO Оптимизировать, без лишних пересчетов
    double min = structSolver.getResultY(0);
    double max = structSolver.getResultY(0);
    for(int i= 0;i<analysis.getMesh().getNodes().size();i++) {
      if(min> structSolver.getResultY(i)) {
        min = structSolver.getResultY(i);
      }
      if(max< structSolver.getResultY(i)) {
        max = structSolver.getResultY(i);
      }
    }
    
    
    double step = (max - min) / 9.0f;
    int color = 0;

    for (double st = min + step; color < 9; st += step, color++)
      if (value <= st) {
        break;
      }

    switch (color) {
      case 0:
        gl.glColor3f(0.0f, 0.0f, 1.0f); // синий
        break;
      case 1:
        gl.glColor3f(0.078f, 0.482f, 0.98f); // светлосиний
        break;
      case 2:
        gl.glColor3f(0.086f, 0.906f, 0.973f); // голубой
        break;
      case 3:
        gl.glColor3f(0.094f, 0.961f, 0.573f); // голубоватый
        break;
      case 4:
        gl.glColor3f(0.0f, 1.0f, 0.0f); // зелёный
        break;
      case 5:
        gl.glColor3f(0.62f, 0.984f, 0.075f); // зеленоватый
        break;
      case 6:
        gl.glColor3f(0.957f, 0.98f, 0.078f); // желтый
        break;
      case 7:
        gl.glColor3f(0.988f, 0.667f, 0.070f); // оранжевый
        break;
      case 8:
        gl.glColor3f(1.0f, 0.0f, 0.0f); // красный
        break;
      default:
        gl.glColor3f(1.0f, 0.0f, 0.0f); // красный
        break;
    }

    return color;
  }

  int setColorThermal(double value) {
    // TODO WARM COLOR CALC
    double maxT = 400;
    double minT = 300;
    
    double step = (maxT - minT) / 9.0f;
    int color = 0;

    for (double st = minT + step; color < 9; st += step, color++)
      if (Math.abs(value) <= Math.abs(st)) {
        break;
      }

    switch (color) {
      case 0:
        gl.glColor3f(0.0f, 0.0f, 1.0f); // синий
        break;
      case 1:
        gl.glColor3f(0.078f, 0.482f, 0.98f); // светлосиний
        break;
      case 2:
        gl.glColor3f(0.086f, 0.906f, 0.973f); // голубой
        break;
      case 3:
        gl.glColor3f(0.094f, 0.961f, 0.573f); // голубоватый
        break;
      case 4:
        gl.glColor3f(0.0f, 1.0f, 0.0f); // зелёный
        break;
      case 5:
        gl.glColor3f(0.62f, 0.984f, 0.075f); // зеленоватый
        break;
      case 6:
        gl.glColor3f(0.957f, 0.98f, 0.078f); // желтый
        break;
      case 7:
        gl.glColor3f(0.988f, 0.667f, 0.070f); // оранжевый
        break;
      case 8:
        gl.glColor3f(1.0f, 0.0f, 0.0f); // красный
        break;
      default:
        gl.glColor3f(1.0f, 0.0f, 0.0f); // красный
        break;
    }

    return color;
  }

}
