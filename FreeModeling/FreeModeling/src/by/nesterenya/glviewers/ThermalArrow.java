package by.nesterenya.glviewers;

import java.util.ArrayList;
import java.util.List;

import javax.media.opengl.GL2;

import by.nesterenya.elements.INode;
import by.nesterenya.elements.INode.Dim;

public class ThermalArrow {

  enum Direction {
    Top, Down, Forward, Back, Left, Right
  };



  public static void draw(GL2 gl, List<INode> nodes, double width, double lenght, String direction)
      throws Exception {
    double r = width / 2.0;
    double rCl = width / 6.0;
    double arrLen = lenght / 3.0;
    double step = Math.PI / 4.0;

    // TODO оптимизировать
    List<PntCircle> pointsCircle = new ArrayList<PntCircle>();

    for (double angle = 0; angle < 2.0 * Math.PI + step; angle += step) {

      double __cos = Math.cos(angle);
      double __sin = Math.sin(angle);
      PntCircle s = new PntCircle(__cos, __sin);

      pointsCircle.add(s);
    }

    // /!Оптимизировать
    switch (direction) {

      case "верхняя":



        for (INode node : nodes) {
          gl.glBegin(gl.GL_TRIANGLE_FAN);

          gl.glVertex3d(node.getValueOfDemention(Dim.X), node.getValueOfDemention(Dim.Y),
              node.getValueOfDemention(Dim.Z));
          for (PntCircle pCr : pointsCircle) {
            gl.glVertex3d(node.getValueOfDemention(Dim.X) + r * pCr.getCos(),
                node.getValueOfDemention(Dim.Y) + r * pCr.getSin(), node.getValueOfDemention(Dim.Z)
                    + arrLen);
          }
          gl.glEnd();

          gl.glBegin(gl.GL_QUAD_STRIP);
          for (PntCircle pCr : pointsCircle) {
            gl.glVertex3d(node.getValueOfDemention(Dim.X) + rCl * pCr.getCos(),
                node.getValueOfDemention(Dim.Y) + rCl * pCr.getSin(),
                node.getValueOfDemention(Dim.Z) + arrLen);
            gl.glVertex3d(node.getValueOfDemention(Dim.X) + rCl * pCr.getCos(),
                node.getValueOfDemention(Dim.Y) + rCl * pCr.getSin(),
                node.getValueOfDemention(Dim.Z) + lenght);
          }
          gl.glEnd();
        }
        break;
      case "нижняя":
        for (INode node : nodes) {
          gl.glBegin(gl.GL_TRIANGLE_FAN);
          gl.glVertex3d(node.getValueOfDemention(Dim.X), node.getValueOfDemention(Dim.Y),
              node.getValueOfDemention(Dim.Z));

          for (PntCircle pCr : pointsCircle) {
            gl.glVertex3d(node.getValueOfDemention(Dim.X) + r * pCr.getCos(),
                node.getValueOfDemention(Dim.Y) + r * pCr.getSin(), node.getValueOfDemention(Dim.Z)
                    - arrLen);
          }

          gl.glEnd();

          gl.glBegin(gl.GL_QUAD_STRIP);
          for (PntCircle pCr : pointsCircle) {
            gl.glVertex3d(node.getValueOfDemention(Dim.X) + rCl * pCr.getCos(),
                node.getValueOfDemention(Dim.Y) + rCl * pCr.getSin(),
                node.getValueOfDemention(Dim.Z) - arrLen);
            gl.glVertex3d(node.getValueOfDemention(Dim.X) + rCl * pCr.getCos(),
                node.getValueOfDemention(Dim.Y) + rCl * pCr.getSin(),
                node.getValueOfDemention(Dim.Z) - lenght);
          }
          gl.glEnd();
        }
        break;
      case "передняя":


        for (INode node : nodes) {
          gl.glBegin(gl.GL_TRIANGLE_FAN);
          gl.glVertex3d(node.getValueOfDemention(Dim.X), node.getValueOfDemention(Dim.Y),
              node.getValueOfDemention(Dim.Z));
          for (PntCircle pCr : pointsCircle) {
            gl.glVertex3d(node.getValueOfDemention(Dim.X) + r * pCr.getCos(),
                node.getValueOfDemention(Dim.Y) + arrLen,
                node.getValueOfDemention(Dim.Z) + r * pCr.getSin());
          }
          gl.glEnd();

          gl.glBegin(gl.GL_QUAD_STRIP);
          for (PntCircle pCr : pointsCircle) {
            gl.glVertex3d(node.getValueOfDemention(Dim.X) + rCl * pCr.getCos(),
                node.getValueOfDemention(Dim.Y) + arrLen, node.getValueOfDemention(Dim.Z) + rCl
                    * pCr.getSin());
            gl.glVertex3d(node.getValueOfDemention(Dim.X) + rCl * pCr.getCos(),
                node.getValueOfDemention(Dim.Y) + lenght, node.getValueOfDemention(Dim.Z) + rCl
                    * pCr.getSin());
          }
          gl.glEnd();
        }
        break;
      case "задняя":

        for (INode node : nodes) {
          gl.glBegin(gl.GL_TRIANGLE_FAN);
          gl.glVertex3d(node.getValueOfDemention(Dim.X), node.getValueOfDemention(Dim.Y),
              node.getValueOfDemention(Dim.Z));
          for (PntCircle pCr : pointsCircle) {
            gl.glVertex3d(node.getValueOfDemention(Dim.X) + r * pCr.getCos(),
                node.getValueOfDemention(Dim.Y) - arrLen,
                node.getValueOfDemention(Dim.Z) + r * pCr.getSin());
          }
          gl.glEnd();

          gl.glBegin(gl.GL_QUAD_STRIP);
          for (PntCircle pCr : pointsCircle) {
            gl.glVertex3d(node.getValueOfDemention(Dim.X) + rCl * pCr.getCos(),
                node.getValueOfDemention(Dim.Y) - arrLen, node.getValueOfDemention(Dim.Z) + rCl
                    * pCr.getSin());
            gl.glVertex3d(node.getValueOfDemention(Dim.X) + rCl * pCr.getCos(),
                node.getValueOfDemention(Dim.Y) - lenght, node.getValueOfDemention(Dim.Z) + rCl
                    * pCr.getSin());
          }
          gl.glEnd();
        }
        break;
      case "правая":

        for (INode node : nodes) {
          gl.glBegin(gl.GL_TRIANGLE_FAN);
          gl.glVertex3d(node.getValueOfDemention(Dim.X), node.getValueOfDemention(Dim.Y),
              node.getValueOfDemention(Dim.Z));
          for (PntCircle pCr : pointsCircle) {
            gl.glVertex3d(node.getValueOfDemention(Dim.X) + arrLen, node.getValueOfDemention(Dim.Y)
                + r * pCr.getCos(), node.getValueOfDemention(Dim.Z) + r * pCr.getSin());
          }
          gl.glEnd();

          gl.glBegin(gl.GL_QUAD_STRIP);
          for (PntCircle pCr : pointsCircle) {
            gl.glVertex3d(node.getValueOfDemention(Dim.X) + arrLen, node.getValueOfDemention(Dim.Y)
                + rCl * pCr.getCos(), node.getValueOfDemention(Dim.Z) + rCl * pCr.getSin());
            gl.glVertex3d(node.getValueOfDemention(Dim.X) + lenght, node.getValueOfDemention(Dim.Y)
                + rCl * pCr.getCos(), node.getValueOfDemention(Dim.Z) + rCl * pCr.getSin());
          }
          gl.glEnd();
        }
        break;
      case "левая":

        for (INode node : nodes) {
          gl.glBegin(gl.GL_TRIANGLE_FAN);
          gl.glVertex3d(node.getValueOfDemention(Dim.X), node.getValueOfDemention(Dim.Y),
              node.getValueOfDemention(Dim.Z));
          for (PntCircle pCr : pointsCircle) {
            gl.glVertex3d(node.getValueOfDemention(Dim.X) - arrLen, node.getValueOfDemention(Dim.Y)
                + r * pCr.getCos(), node.getValueOfDemention(Dim.Z) + r * pCr.getSin());
          }
          gl.glEnd();

          gl.glBegin(gl.GL_QUAD_STRIP);
          for (PntCircle pCr : pointsCircle) {
            gl.glVertex3d(node.getValueOfDemention(Dim.X) - arrLen, node.getValueOfDemention(Dim.Y)
                + rCl * pCr.getCos(), node.getValueOfDemention(Dim.Z) + rCl * pCr.getSin());
            gl.glVertex3d(node.getValueOfDemention(Dim.X) - lenght, node.getValueOfDemention(Dim.Y)
                + rCl * pCr.getCos(), node.getValueOfDemention(Dim.Z) + rCl * pCr.getSin());
          }
          gl.glEnd();
        }

        break;
    }

  }


}


class PntCircle {
  private double _cos;
  private double _sin;

  public PntCircle(double incos, double insin) {
    this.setCos(incos);
    this.setSin(insin);
  }

  public double getCos() {
    return _cos;
  }

  public void setCos(double incos) {
    this._cos = incos;
  }

  public double getSin() {
    return _sin;
  }

  public void setSin(double insin) {
    this._sin = insin;
  }
}
