package by.nesterenya.analysis;

public class Result implements IDynamicResult{
  private int selectedTimeLayer;
  private int timeLayersCount;
  
  public Result(int timeLayersCount) {
    this.timeLayersCount = timeLayersCount;
  }
  
  public int getSelectedTimeLayer() {
    return selectedTimeLayer;
  }

//  public void setSelectedTimeLayer(int selectedTimeLayer) {
//    this.selectedTimeLayer = selectedTimeLayer;
//  }
  
  public void nextTimeLayer() {
    if(selectedTimeLayer<timeLayersCount) selectedTimeLayer++; 
    else selectedTimeLayer = 0;
  }
  
  public void previousTimeLayer() {
    if(selectedTimeLayer>0) selectedTimeLayer--;
    else selectedTimeLayer = timeLayersCount-1;
  }
  
}
