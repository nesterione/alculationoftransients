package by.nesterenya.analysis;

import java.util.List;

import by.nesterenya.boundary.ILoad;
import by.nesterenya.geometry.Box;
import by.nesterenya.mesh.Mesh;

public class StaticStructalAnalisis extends Analysis{

  //TODO Сделать что бы грани хранились в списке в фигуре, и тамже хранилась выделенная грань
  private String selectedPlane = "";
  
  public DataInitStaticStructal getDataInit() {
    return (DataInitStaticStructal) dataInit;
  }
  
  public void setDataInit(DataInitStaticStructal initData) {
    this.dataInit = initData;
  }
  
  public String getSelectedPlane() {
    return this.selectedPlane;
  }
  
  public void setSelectedPlane(String selectedPlane) {
    this.selectedPlane = selectedPlane;
  }
  
  private Result rezult;

  public Box getGeometry() {
    return geometry;
  }

  public void setGeometry(Box geometry) {
    this.geometry = geometry;
  }

  public Mesh getMesh() {
    return mesh;
  }

  public void setMesh(Mesh mesh) {
    this.mesh = mesh;
  }

  public List<ILoad> getLoads() {
    return loads;
  }

  public void setLoads(List<ILoad> loads) {
    this.loads = loads;
  }

  public IResult getResult() {
    return result;
  }

  public void setResult(Result result) {
    this.result = result;
  }
  
  @Override
  public void generateMesh() {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void solve() {
    // TODO Auto-generated method stub
    
  }

}
