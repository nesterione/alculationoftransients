package by.nesterenya.analysis;

public class DataInitStaticStructal implements IDataInit {
  
  //TODO пока массив, потом подумать как заменить
  private double[] temperature;
  
  public DataInitStaticStructal(double[] temperature) {
    this.temperature = temperature;
  }
  
  public double[] getTemperature() {
    return temperature;
  }
  
  public double getTemperature(int numberNode) {
    return temperature[numberNode];
  }
  
}
