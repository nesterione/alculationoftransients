package by.nesterenya.analysis;

import java.util.ArrayList;
import java.util.List;

import by.nesterenya.boundary.ILoad;
import by.nesterenya.geometry.Box;
import by.nesterenya.mesh.Mesh;

/**
 * Базовый класс для любых типо исследований
 * @author igor
 *
 */
 public abstract class Analysis {
  //TODO переместить границы в geometry
  // Геометрия модели
  protected Box geometry;
  
  // Список нагрузок
  protected List<ILoad> loads = new ArrayList<>();
  
  // Сетка 
  protected Mesh mesh;
  
  
  protected IDataInit dataInit;
  
  //Результат исследования
  protected IResult result;
  
  public abstract List<ILoad> getLoads();
  public abstract Box getGeometry();
  public abstract Mesh getMesh();
  public abstract IResult getResult();
  
  //TODO этот метод суда не вписывается, подумать куда переместить
  public abstract String getSelectedPlane();
  
  public abstract void generateMesh();
  public abstract void solve();
}
