package by.nesterenya.analysis;

public class DataInitThermalDynamic implements IDynamicInitData, IDataInit {
  private double timeStudy;
  private double timeStep;
  private double initialThemperature;
  
  public DataInitThermalDynamic(double timeStudy, double timeStep, double initialThemperature) {
    setTimeStudy(timeStudy);
    setTimeStep(timeStep);
    setInitialThemperature(initialThemperature);
  }
  
  @Override
  public double getTimeStudy() {
    return timeStudy;
  }

  @Override
  public void setTimeStudy(double time) {
    timeStudy = time;
  }

  @Override
  public double getTimeStep() {
    return timeStep;
  }

  @Override
  public void setTimeStep(double time) {
    timeStep = time;
  }
  

  public double getInitialThemperature() {
    return initialThemperature;
  }
  

  public void setInitialThemperature(double initialThemperature) {
    this.initialThemperature = initialThemperature;
  }
  
}
