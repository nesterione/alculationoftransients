package by.nesterenya.analysis;

public interface IDynamicResult extends IResult{
  public int getSelectedTimeLayer();
  public void nextTimeLayer();
  public void previousTimeLayer();
}
