package by.nesterenya.analysis;

public interface IDynamicInitData {
  public double getTimeStudy();
  public void setTimeStudy(double time);
  public double getTimeStep();
  public void setTimeStep(double time);
}
