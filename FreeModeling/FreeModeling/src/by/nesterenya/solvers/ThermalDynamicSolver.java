package by.nesterenya.solvers;

import java.io.IOException;
import java.util.List;

import by.nesterenya.analysis.DataInitThermalDynamic;
import by.nesterenya.analysis.Result;
import by.nesterenya.analysis.ThermalDynamicAnalisis;
import by.nesterenya.boundary.Boundary;
import by.nesterenya.boundary.ILoad;
import by.nesterenya.boundary.StaticTemperature;
import by.nesterenya.elements.IElement;
import by.nesterenya.elements.INode;
import by.nesterenya.elements.INode.Dim;
import by.nesterenya.elements.material.Material;
import by.nesterenya.io.FileOperation;

/**
 * Решатель для динамической задачи теплопроводности
 * 
 * @author igor
 *
 */
public class ThermalDynamicSolver {
  // TODO нормальные коменты

  /**
   * Решение динамической задачи теплопроводности
   * 
   * 
   * @param thermalAnalis Объект с параметрами температурной задачи
   */
  public ThermalDynamicSolver(ThermalDynamicAnalisis thermalAnalis) {
    this.analis = thermalAnalis;
  }

  private final int RANK_LOKAL_H = 4;

  private ThermalDynamicAnalisis analis;


  // *Mesh mesh;

  // / <summary>
  // / Вектор тепла
  // / </summary>
  protected double[] R;

  // double k;

  // double c;

  // double ro;

  public double[][] T;

  private final int RANK_B_COL = 4;
  private final int RANK_B_ROW = 3;
  private final int RANK_C = 4;

  // Формирование координатной матрицы Mx1 для заданных координт
  private double[][] formMatrixA(double x1, double y1, double z1, double x2, double y2, double z2,
      double x3, double y3, double z3, double x4, double y4, double z4) {
    double retH[][] = new double[RANK_LOKAL_H][RANK_LOKAL_H];

    // Заполняем верхнюю половину матрицы
    retH[0][0] = 1;
    retH[0][1] = x1;
    retH[0][2] = y1;
    retH[0][3] = z1;
    retH[1][0] = 1;
    retH[1][1] = x2;
    retH[1][2] = y2;
    retH[1][3] = z2;
    retH[2][0] = 1;
    retH[2][1] = x3;
    retH[2][2] = y3;
    retH[2][3] = z3;
    retH[3][0] = 1;
    retH[3][1] = x4;
    retH[3][2] = y4;
    retH[3][3] = z4;

    return retH;
  }

  protected double[][] formMatrixB() {
    double[][] retB = new double[RANK_B_ROW][RANK_B_COL];

    retB[0][1] = 1;
    retB[1][2] = 1;
    retB[2][3] = 1;

    return retB;
  }
//TODO удалить
int co = 0;


  protected double[][] formLocalMxH(double x1, double y1, double z1, double x2, double y2,
      double z2, double x3, double y3, double z3, double x4, double y4, double z4, int elemNum)
      throws Exception {
    
    double[][] B = formMatrixB();
    double[][] Mx1 = formMatrixA(x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4);
    
    // Вычисляем объем элемента
    double[][] md = { {1, x1, y1, z1}, {1, x2, y2, z2}, {1, x3, y3, z4}, {1, x4, y4, z4}};

    double Ve = Math.abs(MMath.DET(md)) / 6.0;

    double[][] Q = MMath.MUL(B, MMath.INV(Mx1));// TODO Warning



    double[][] H =
        MMath.MUL(
            MMath.MUL(MMath.T(Q), Q),
            Ve
                * ((Material) analis.getMesh().getElements().get(elemNum).getMatherial())
                    .getThermalConductivity());


    

    
    

    
    /*
     * mesh.getMaterial(analis.getMesh().getElements().get(elemNum)).thermalConductivity][);
     */
    return H;
  }

  protected double[][] formLocalMxC(int numElement) throws Exception {
    double[][] retC = new double[RANK_C][RANK_C];

    IElement element = analis.getMesh().getElements().get(numElement);

    INode node0 = element.getNode(0);
    INode node1 = element.getNode(1);
    INode node2 = element.getNode(2);
    INode node3 = element.getNode(3);

    // Вычисляем объем элемента
    double[][] md =
        {
            {1, node0.getValueOfDemention(Dim.X), node0.getValueOfDemention(Dim.Y),
                node0.getValueOfDemention(Dim.Z)},
            {1, node1.getValueOfDemention(Dim.X), node1.getValueOfDemention(Dim.Y),
                node1.getValueOfDemention(Dim.Z)},
            {1, node2.getValueOfDemention(Dim.X), node2.getValueOfDemention(Dim.Y),
                node2.getValueOfDemention(Dim.Z)},
            {1, node3.getValueOfDemention(Dim.X), node3.getValueOfDemention(Dim.Y),
                node3.getValueOfDemention(Dim.Z)}};

    double Ve = Math.abs(MMath.DET(md)) / 6.0;
    // (mesh.getMaterial(mesh.elements[numElement]).specificHeatCapacity *
    // mesh.getMaterial(mesh.elements[numElement]).density * Ve) / 20.0;

    Material material = ((Material) analis.getMesh().getElements().get(numElement).getMatherial());
    double mnoj = (material.getSpecificHeatCapacity() * material.getDensity() * Ve) / 20.0;

    // double[,] C = {
    // {2.0,1.0,1.0,1.0},
    // {1.0,2.0,1.0,1.0},
    // {1.0,1.0,2.0,1.0},
    // {1.0,1.0,1.0,2.0},
    // };

    double[][] C = { {1, 0, 0, 0}, {0, 1, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, 1}};

    return MMath.MUL(C, mnoj);
  }

  // / <summary>
  // / Количество узлов в элементе
  // / </summary>
  final int COUNT_NODES = 4;

  // / <summary>
  // / Количество степеней свободы в узле
  // / </summary>
  final int DEGREES_OF_FREEDOM = 1;

  public void formAllGlovalMatrix() throws Exception {
    // размерность глобальной матрицы равна количеству узлов
    // / <summary>
    // / Глобальная матрица теплопроводности
    // / </summary>
    double[][] gH =
        new double[analis.getMesh().getNodes().size()][analis.getMesh().getNodes().size()];
    double[][] gC =
        new double[analis.getMesh().getNodes().size()][analis.getMesh().getNodes().size()];

    // Цикл обходяций все конечные элементы
    // TODO WARN
    for (int i = 0; i < analis.getMesh().getElements().size()/* getNodes().size() */; i++) {

      IElement element = analis.getMesh().getElements().get(i);
      INode node0 = element.getNode(0);
      INode node1 = element.getNode(1);
      INode node2 = element.getNode(2);
      INode node3 = element.getNode(3);

      // TODO убрать эти грабли
      double[][] H =
          formLocalMxH(node0.getValueOfDemention(Dim.X), node0.getValueOfDemention(Dim.Y),
              node0.getValueOfDemention(Dim.Z), node1.getValueOfDemention(Dim.X),
              node1.getValueOfDemention(Dim.Y), node1.getValueOfDemention(Dim.Z),
              node2.getValueOfDemention(Dim.X), node2.getValueOfDemention(Dim.Y),
              node2.getValueOfDemention(Dim.Z), node3.getValueOfDemention(Dim.X),
              node3.getValueOfDemention(Dim.Y), node3.getValueOfDemention(Dim.Z), i);

      double[][] C = formLocalMxC(i);

      //
      // Формирование глобальных матриц
      //
      for (int si = 0; si < COUNT_NODES; si++)
        for (int sj = 0; sj < COUNT_NODES; sj++)
          for (int ki = 0; ki < DEGREES_OF_FREEDOM; ki++)
            for (int kj = 0; kj < DEGREES_OF_FREEDOM; kj++) {
              IElement elem = analis.getMesh().getElements().get(i);
              // TODO Возможна ошибка при нахождении индекса в коллекции
              int ind_si = analis.getMesh().getNodes().lastIndexOf(elem.getNode(si));
              int ind_sj = analis.getMesh().getNodes().lastIndexOf(elem.getNode(sj));

              gH[ind_si * DEGREES_OF_FREEDOM + ki][ind_sj * DEGREES_OF_FREEDOM + kj] +=
                  H[si * DEGREES_OF_FREEDOM + ki][sj * DEGREES_OF_FREEDOM + kj];

              gC[ind_si * DEGREES_OF_FREEDOM + ki][ind_sj * DEGREES_OF_FREEDOM + kj] +=
                  C[si * DEGREES_OF_FREEDOM + ki][sj * DEGREES_OF_FREEDOM + kj];
            }
    }

    // FilesWork.PrintMatrixToFile(gH, "gH.txt");
    // FilesWork.PrintMatrixToFile(gC, "gC.txt");

    // Глобальные матрицы K и C сформировоны
    // Получить матрицы Mx1 и Mx2
    // Mx1 = 2*[gC]/stepTime + [gK] //B
    // Mx2 = 2*[gC]/stepTime - [gK] //P

    double mn = 2.0 / analis.getDataInit().getTimeStep();
    Mx1 = MMath.SUM(MMath.MUL(gC, mn), gH);
    Mx2 = MMath.SUM(MMath.MUL(gC, mn), MMath.MUL(gH, -1.0));

    // Инициация вектора тепловых нагрузок
    // R = new double[gH.lenght];
  }

  double[][] Mx1;// B
  double[][] Mx2;// P


  // public void setTemperature(List<int> loadNodes, double Temperature)
  // {
  // //double dLoad = load * 0.05 /** S*/ / loadNodes.Count;//вычисление нагрузки приходящейся на
  // каждый узел

  // for (int i = 0; i < loadNodes.Count; i++)
  // {
  // R[loadNodes[i] * DEGREES_OF_FREEDOM] = Temperature;


  // for (int ii = 0; ii < gH.lenght; ii++)
  // {
  // gH[loadNodes[i] * DEGREES_OF_FREEDOM, ii] = 0;
  // //gH[ii, loadNodes[i] * DEGREES_OF_FREEDOM] = 0;

  // }
  // gH[loadNodes[i] * DEGREES_OF_FREEDOM, loadNodes[i] * DEGREES_OF_FREEDOM] = 1;
  // }
  // }

  public void Solve() {   
    DataInitThermalDynamic dataInit = analis.getDataInit();
    int cntTst = (int) (dataInit.getTimeStudy() / dataInit.getTimeStep());
    cntTst++;

    //TODO запихнуть в analis
    T = new double[Mx1.length][cntTst];
    
    // Указание начальнй температуры
    for (int i = 0; i < T.length; i++)
      T[i][0] = dataInit.getInitialThemperature();

    for(ILoad boundary : analis.getLoads()) {
      for(INode node :boundary.getBoundary().getNodes())
      T[getGlobalNumberNode(node)][0] = ((StaticTemperature)boundary).getTemperature();
    }
      
    //T[0].length-1 количества временных слоев
    analis.setResult(new Result(T[0].length-1));
    
    for (int JJ = 0; JJ < T[0].length - 1; JJ++) {
      double[] T0 = new double[T.length];
      for (int jj = 0; jj < T.length; jj++) {
        T0[jj] = T[jj][JJ];
      }

      double[] RPart = MMath.MUL(Mx2, T0);
      
      for(ILoad boundary : analis.getLoads()) {
        for (INode node :boundary.getBoundary().getNodes()) {  
          RPart[getGlobalNumberNode(node) * DEGREES_OF_FREEDOM] = ((StaticTemperature)boundary).getTemperature();;
  
          for (int ii = 0; ii < Mx1.length; ii++) {
            Mx1[getGlobalNumberNode(node) * DEGREES_OF_FREEDOM][ii] = 0;
          }
          Mx1[getGlobalNumberNode(node) * DEGREES_OF_FREEDOM][getGlobalNumberNode(node) * DEGREES_OF_FREEDOM] = 1;
        }
      }

      double[] T1;
      try {
        T1 = MMath.gausSLAU_NCH(Mx1, RPart);
        for (int jj = 0; jj < T1.length; jj++) {
          T[jj][JJ + 1] = T1[jj];
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  private int getGlobalNumberNode(INode node) {
    //TODO добавить обработку ошибок
    return analis.getMesh().getNodes().lastIndexOf(node);
  }
  
  public double getResultX(int i) {
    return 16655;// T[i];
  }

}
