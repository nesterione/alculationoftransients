package by.nesterenya.solvers;

import java.util.List;

import by.nesterenya.analysis.DataInitStaticStructal;
import by.nesterenya.analysis.StaticStructalAnalisis;
import by.nesterenya.analysis.ThermalDynamicAnalisis;
import by.nesterenya.boundary.EvenlyDistributedLoad;
import by.nesterenya.boundary.ILoad;
import by.nesterenya.boundary.Support;
import by.nesterenya.elements.IElement;
import by.nesterenya.elements.INode;
import by.nesterenya.elements.INode.Dim;
import by.nesterenya.elements.material.Material;

public class StaticStructalSolver {
  
  private StaticStructalAnalisis analisis;
  public StaticStructalSolver(StaticStructalAnalisis analisis) {
    this.analisis = analisis;
  }
  // private const int RANK_LOKAL_H = 4;

  // координатная матрица
  double[][] A;

  // инвертированная координатная матрица
  double[][] B;

  // матрица
  double[][] Q;

  // локальная матрица жёсткости
  double[][] K;

  // глобальная матрица жесткости
  double[][] gK;


  // формирование координатной матрицы А
  public void formMatrixA(double x0, double y0, double z0, double x1, double y1, double z1,
      double x2, double y2, double z2, double x3, double y3, double z3) {
    A = new double[12][12];
    double[] kX = new double[4];
    kX[0] = x0;
    kX[1] = x1;
    kX[2] = x2;
    kX[3] = x3;
    double[] kY = new double[4];
    kY[0] = y0;
    kY[1] = y1;
    kY[2] = y2;
    kY[3] = y3;
    double[] kZ = new double[4];
    kZ[0] = z0;
    kZ[1] = z1;
    kZ[2] = z2;
    kZ[3] = z3;

    for (int i = 0; i < 4; i++) {
      A[i * 3][0] = 1.0f;
      A[i * 3][1] = kX[i];
      A[i * 3][2] = kY[i];
      A[i * 3][3] = kZ[i];

      A[i * 3 + 1][4] = 1.0f;
      A[i * 3 + 1][5] = kX[i];
      A[i * 3 + 1][6] = kY[i];
      A[i * 3 + 1][7] = kZ[i];

      A[i * 3 + 2][8] = 1.0f;
      A[i * 3 + 2][9] = kX[i];
      A[i * 3 + 2][10] = kY[i];
      A[i * 3 + 2][11] = kZ[i];
    }
  }

  public void formMatrixQ() {
    Q = new double[6][12];
    
    Q[0][1] = 1.0f;
    Q[1][6] = 1.0f;
    Q[2][11] = 1.0f;
    Q[3][2] = 1.0f;
    Q[3][5] = 1.0f;
    Q[4][7] = 1.0f;
    Q[4][10] = 1.0f;
    Q[5][3] = 1.0f;
    Q[5][9] = 1.0f;
  }

  // Формирование матрицы E
  public double[][] formMatrixE(double locG, double locMy) {
    // locG - модуль Юнга
    // locMy - коэффициет Пуассона

    double kafE = (locG * (1 - locMy)) / ((1 + locMy) * (1 - 2.0f * locMy));

    double[][] E = new double[6][6];


    E[0][0] = 1.0f;
    E[0][1] = locMy / (1 - locMy);
    E[0][2] = locMy / (1 - locMy);

    E[1][0] = locMy / (1 - locMy);
    E[1][1] = 1;
    E[1][2] = locMy / (1 - locMy);

    E[2][0] = locMy / (1 - locMy);
    E[2][1] = locMy / (1 - locMy);
    E[2][2] = 1.0f;

    E[3][3] = (1.0f - 2.0f * locMy) / (2.0f * (1.0f - locMy));

    E[4][4] = (1.0f - 2.0f * locMy) / (2.0f * (1.0f - locMy));

    E[5][5] = (1.0f - 2.0f * locMy) / (2.0f * (1.0f - locMy));

    E = MMath.MUL(E, kafE);

    return E;
  }



  // const int RANK_B_COL = 4;
  // const int RANK_B_ROW = 3;

  // protected double[,] formMatrixB()
  // {
  // double[,] retB = new double[RANK_B_ROW, RANK_B_COL];
  //
  // retB[0, 1] = 1;
  // retB[1, 2] = 1;
  // retB[2, 3] = 1;
  //
  // return retB;
  // }


  // / <summary>
  // / Вектор нагрузок
  // / </summary>
  private double[] R;

  double k;



  // / <summary>
  // / Количество узлов в элементе
  // / </summary>
  final int COUNT_NODES = 4;

  // / <summary>
  // / Количество степеней свободы в узле
  // / </summary>
  final int DEGREES_OF_FREEDOM = 3;

  private double[] eps0(double T, double alp) {
    double[] eps = new double[6];
    
    eps[0] = alp*T;
    eps[1] = alp*T;
    eps[2] = alp*T;
    eps[3] = 0;
    eps[4] = 0;
    eps[5] = 0;
    
    return eps;
  }
  
  public void formGlobalK() throws Exception {
    List<IElement> elements = analisis.getMesh().getElements();
    List<INode> nodes = analisis.getMesh().getNodes();

    formMatrixQ();

    // Инициализируем локальную матрицу жесткости
    gK = new double[nodes.size() * 3][nodes.size() * 3];
    R = new double[gK.length];


    // Для всех элементов
    for (IElement element : elements) {
      INode node0 = element.getNode(0);
      INode node1 = element.getNode(1);
      INode node2 = element.getNode(2);
      INode node3 = element.getNode(3);

      Material material = (Material) element.getMatherial();

      // TODO WARM в параметрах
      double[][] curE = formMatrixE(material.getElasticModulus(), material.getPoissonsRatio());

      // Формируем координатную матрицу для текущего элемента
      formMatrixA(node0.getValueOfDemention(Dim.X), node0.getValueOfDemention(Dim.Y),
          node0.getValueOfDemention(Dim.Z),

          node1.getValueOfDemention(Dim.X), node1.getValueOfDemention(Dim.Y),
          node1.getValueOfDemention(Dim.Z),

          node2.getValueOfDemention(Dim.X), node2.getValueOfDemention(Dim.Y),
          node2.getValueOfDemention(Dim.Z),

          node3.getValueOfDemention(Dim.X), node3.getValueOfDemention(Dim.Y),
          node3.getValueOfDemention(Dim.Z));

      B = MMath.INV(A);

      // #region Вычисляем объем текущего элемента


      // Вычисляем объем элемента
      double[][] md =
          {
              {1, node0.getValueOfDemention(Dim.X), node0.getValueOfDemention(Dim.Y),
                  node0.getValueOfDemention(Dim.Z)},
              {1, node1.getValueOfDemention(Dim.X), node1.getValueOfDemention(Dim.Y),
                  node1.getValueOfDemention(Dim.Z)},
              {1, node2.getValueOfDemention(Dim.X), node2.getValueOfDemention(Dim.Y),
                  node2.getValueOfDemention(Dim.Z)},
              {1, node3.getValueOfDemention(Dim.X), node3.getValueOfDemention(Dim.Y),
                  node3.getValueOfDemention(Dim.Z)}};

      double Ve = Math.abs(MMath.DET(md)) / 6.0;
      // #endregion

      // #region Формирование локальной матрицы жесткости
      K = MMath.MUL(MMath.T(B), MMath.T(Q));
      K = MMath.MUL(K, curE);
      K = MMath.MUL(K, Q);
      K = MMath.MUL(K, B);
      K = MMath.MUL(K, Ve);
      // #endregion

      
      if(analisis.getDataInit()!=null) {
        //TODO warm warm warm
        //TODO Оптимизировать
        
        int numberNode0  = analisis.getMesh().getNodes().lastIndexOf(node0);
        int numberNode1  = analisis.getMesh().getNodes().lastIndexOf(node1);
        int numberNode2  = analisis.getMesh().getNodes().lastIndexOf(node2);
        int numberNode3  = analisis.getMesh().getNodes().lastIndexOf(node3);
        
        double T0 =  ((DataInitStaticStructal)analisis.getDataInit()).getTemperature(numberNode0);
        double T1 =  ((DataInitStaticStructal)analisis.getDataInit()).getTemperature(numberNode1);
        double T2 =  ((DataInitStaticStructal)analisis.getDataInit()).getTemperature(numberNode2);
        double T3 =  ((DataInitStaticStructal)analisis.getDataInit()).getTemperature(numberNode3);
        
        //Температура элемента, усредненная температура узлов
        //TODO внимание здесь отнимается 300 кельвинов , нужно отнимать начальную температуру, 
        //Добавить в начальные данные исходную температуру
        double elementT = ((T0+T1+T2+T3)/4.0)- 300;
        
        double[] eps = eps0(elementT, material.getThermalExpansion());
        //TODO Оптимизировать
        double[] F =  MMath.MUL(MMath.MUL(MMath.MUL(MMath.MUL(MMath.T(B), MMath.T(Q)),curE),eps),Ve);
      
        for(int si = 0; si< COUNT_NODES; si++) {
          for(int ki=0; ki< DEGREES_OF_FREEDOM; ki++) {
            // TODO Возможна ошибка при нахождении индекса в коллекции
            int ind_si = analisis.getMesh().getNodes().lastIndexOf(element.getNode(si));
            R[ind_si*DEGREES_OF_FREEDOM+ki]+=F[si*DEGREES_OF_FREEDOM+ ki];
          }
        }
      }
      
      
      
      // Записуем текущую локальную матрицу в глобальную
      for (int si = 0; si < COUNT_NODES; si++)
        for (int sj = 0; sj < COUNT_NODES; sj++)
          for (int ki = 0; ki < DEGREES_OF_FREEDOM; ki++)
            for (int kj = 0; kj < DEGREES_OF_FREEDOM; kj++) {

              // TODO Возможна ошибка при нахождении индекса в коллекции
              int ind_si = analisis.getMesh().getNodes().lastIndexOf(element.getNode(si));
              int ind_sj = analisis.getMesh().getNodes().lastIndexOf(element.getNode(sj));

              gK[ind_si * DEGREES_OF_FREEDOM + ki][ind_sj * DEGREES_OF_FREEDOM + kj] +=
                  K[si * DEGREES_OF_FREEDOM + ki][sj * DEGREES_OF_FREEDOM + kj];

              // gK[elements[i].uz[si] * 3 + ki, elements[i].uz[sj] * 3 + kj] += K[si * 3 + ki, sj *
              // 3 + kj];
            }
    }

    
  }

  public void setBoundaries() {

    // Важно чтобы фиксация происходила после указания нагрузок
    for (ILoad load : analisis.getLoads()) {
      if (load instanceof EvenlyDistributedLoad) {
        addLoad((EvenlyDistributedLoad) load);
      }
    }

    // Фиксируем грани
    for (ILoad load : analisis.getLoads()) {
      if (load instanceof Support) {
        fixNodes(load.getBoundary().getNodes());
      }
    }

  }

  private void fixNodes(List<INode> fixedNodes) {
    for (INode node : fixedNodes) {
      int numberFixedNode = analisis.getMesh().getNodes().lastIndexOf(node);

      for (int j = 0; j < 3; j++) {
        for (int kk = 0; kk < gK.length; kk++) {
          gK[numberFixedNode * 3 + j][kk] = 0;
          gK[kk][numberFixedNode * 3 + j] = 0;
        }

        R[numberFixedNode * 3 + j] = 0;
        gK[numberFixedNode * 3 + j][numberFixedNode * 3 + j] = 1;
      }
    }
  }

  private void addLoad(EvenlyDistributedLoad distrubutedLoad) {

    double nodeLoad = distrubutedLoad.getLoad() / distrubutedLoad.getSquare();

    for (INode node : distrubutedLoad.getBoundary().getNodes()) {
      // TODO Сделать чтобы при добавлении температуры значения доже прибавлялиьс

      // Значение должно прибовлятся к существующему, так как на некоторые узлы уже может быть
      // оказана нагрузка
      R[(analisis.getMesh().getNodes().lastIndexOf(node)*DEGREES_OF_FREEDOM)+2] += nodeLoad;
    }
  }

  public void Solve() {
    try {
      formGlobalK();
      setBoundaries();
      R = MMath.gausSLAU(gK, R);
    } catch (Exception e) {
      // TODO Выбрасывать вверх
      e.printStackTrace();
    }
  }

  
  
  public double getResultX(int i) {

    return R[i * DEGREES_OF_FREEDOM];
  }

  public double getResultY(int i) {
    return R[i * DEGREES_OF_FREEDOM + 1];
  }

  public double getResultZ(int i) {
    return R[i * DEGREES_OF_FREEDOM + 2];
  }
}
