package by.nesterenya.domainmodal.dal;

import java.util.List;
/**
 * 
 * @author igor
 * Интерфейс преданазначед для выполнения CRUD операций
 *
 * @param <T> Класс модели предметной области
 */
public interface IRepository<T> {
  public int create(T model);
  public T get(int id);
  public List<T> getAll();
  public void delete(T model);
  public void update(T model);
}
