package by.nesterenya.domainmodal.dal;
//TODO переделать что бы в классах из БД использовался мой класс в качестве Id
public class ModelId {
  private int id;
   
  public int getId() {
    return id;
  }
  
  public ModelId(int id) {
    this.id = id;
  }
  
  @Override
  public int hashCode() {
    return id;
  }
  
  @Override
  public boolean equals(Object obj) {
   if(obj == this) 
     return true;
   
   if (!(obj instanceof ModelId))
     return false;
   
   ModelId modelId = (ModelId)obj;
     return (this.id == modelId.getId());
  }
}
