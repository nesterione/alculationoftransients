package by.nesterenya.io;

import java.io.*;

public class FileOperation {
  public static void writeMatrix(double[][] matrix, String filename) throws IOException {
    File file = new File(filename);
    FileWriter writer = new FileWriter(file);
    
    //flt = new File("javaprobooks.txt");
    //PrintWriter out = new PrintWriter(new BufferedWriter(
    //new FileWriter(flt)));
    //out.print("Welcome to javaprobooks.ru");
    //out.flush();
    
    for(int i = 0; i<matrix.length;i++) {
      for(int j = 0; j< matrix[0].length; j++) {
        CharSequence sequence = String.format(" %f ", matrix[i][j]);
        writer.append(sequence);
      }
      writer.append("\n");
    }
    writer.flush();
    writer.close();
  }
}
