package by.nesterenya.boundary;


public class EvenlyDistributedLoad implements ILoad {
  private double load;
  private Boundary boundary;

  public EvenlyDistributedLoad(double load, Boundary boundary) {
    this.setLoad(load);
    this.setBoundary(boundary);
  }

  public double getLoad() {
    return load;
  }

  public void setLoad(double load) {
    this.load = load;
  }

  public Boundary getBoundary() {
    return boundary;
  }

  public void setBoundary(Boundary boundary) {
    this.boundary = boundary;
  }

  public double getSquare() {
    return boundary.getSquare();
  }
  
  @Override
  public String toString() {
    return boundary.getName() + ": нагрузка " + load + " Па";
  }
}
